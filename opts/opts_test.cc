#include "opts.h"

#include <iostream>

int main(int argc, char** argv) {
  cxxopts::Options options;
  options
  .add_option("f", "", "File", cxxopts::value<std::vector<std::string>>())
  .add_option("p", "", "Power", cxxopts::value<std::vector<std::string>>())
  .add_option("t", "", "Time", cxxopts::value<int>()->default_value("-1"))
  .add_option("m", "", "Memory", cxxopts::value<int>()->default_value("-1"));
  options.parse(argc, argv);
  if (options.count("f")) {
    auto& ff = options["f"].as<std::vector<std::string>>();
    std::cout << "Files" << std::endl;
    for (const auto& f : ff) {
      std::cout << f << std::endl;
    }
  }
  if (options.count("p")) {
    auto& ff = options["p"].as<std::vector<std::string>>();
    std::cout << "Power" << std::endl;
    for (const auto& f : ff) {
      std::cout << f << std::endl;
    }
  }
  int time = options["t"].as<int>();
  std::cout << "time: " << time << std::endl;
  int memory = options["m"].as<int>();
  std::cout << "memory: " << memory << std::endl;
  return 0;
}

