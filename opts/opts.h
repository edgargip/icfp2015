/*
Copyright (c) 2014 Jarryd Beck
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef CXX_OPTS_HPP
#define CXX_OPTS_HPP

#include <exception>
#include <iostream>
#include <map>
#include <memory>
#include <regex>
#include <sstream>
#include <string>
#include <vector>

namespace cxxopts {
class Value : public std::enable_shared_from_this<Value> {
 public:
  virtual void parse(const std::string& text) const = 0;

  virtual void parse() const = 0;

  virtual bool has_arg() const = 0;

  virtual bool has_default() const = 0;

  virtual std::string get_default_value() const = 0;

  virtual std::shared_ptr<Value> default_value(const std::string& value) = 0;
};

class OptionException : public std::exception {
 public:
  OptionException(const std::string& message) : m_message(message) {}

  virtual const char* what() const noexcept { return m_message.c_str(); }

 private:
  std::string m_message;
};

class OptionSpecException : public OptionException {
 public:
  OptionSpecException(const std::string& message) : OptionException(message) {}
};

class OptionParseException : public OptionException {
 public:
  OptionParseException(const std::string& message) : OptionException(message) {}
};

class option_exists_error : public OptionSpecException {
 public:
  option_exists_error(const std::string& option)
      : OptionSpecException("Option " + option + " already exists") {}
};

class invalid_option_format_error : public OptionSpecException {
 public:
  invalid_option_format_error(const std::string& format)
      : OptionSpecException("Invalid option format " + format) {}
};

class option_not_exists_exception : public OptionParseException {
 public:
  option_not_exists_exception(const std::string& option)
      : OptionParseException("Option " + option + " does not exist") {}
};

class missing_argument_exception : public OptionParseException {
 public:
  missing_argument_exception(const std::string& option)
      : OptionParseException("Option " + option + " is missing an argument") {}
};

class option_requires_argument_exception : public OptionParseException {
 public:
  option_requires_argument_exception(const std::string& option)
      : OptionParseException("Option " + option +
                             " requires an argument") {}
};

class option_not_has_argument_exception : public OptionParseException {
 public:
  option_not_has_argument_exception(const std::string& option,
                                    const std::string& arg)
      : OptionParseException("Option " + option +
                             " does not take an argument, but argument" +
                             arg + " given") {}
};

class option_not_present_exception : public OptionParseException {
 public:
  option_not_present_exception(const std::string& option)
      : OptionParseException("Option " + option + " not present") {}
};

class argument_incorrect_type : public OptionParseException {
 public:
  argument_incorrect_type(const std::string& arg)
      : OptionParseException("Argument " + arg + " failed to parse") {}
};

namespace values {

inline void parse_value(const std::string& /*text*/, bool& value) {
  // TODO recognise on, off, yes, no, enable, disable
  // so that we can write --long=yes explicitly
  value = true;
}

inline void parse_value(const std::string& text, std::string& value) {
  value = text;
}

template <typename T>
void parse_value(const std::string& text, T& value) {
  std::istringstream is(text);
  if (!(is >> value)) {
    std::cerr << "cannot parse empty value" << std::endl;
    throw argument_incorrect_type(text);
  }

  if (is.rdbuf()->in_avail() != 0) {
    throw argument_incorrect_type(text);
  }
}

template <typename T>
void parse_value(const std::string& text, std::vector<T>& value) {
  T v;
  parse_value(text, v);
  value.push_back(v);
}

template <typename T>
struct value_has_arg {
  static constexpr bool value = true;
};

template <>
struct value_has_arg<bool> {
  static constexpr bool value = false;
};

template <typename T>
class standard_value : public Value {
 public:
  standard_value() : m_result(std::make_shared<T>()), m_store(m_result.get()) {}

  standard_value(T* t) : m_store(t) {}

  void parse(const std::string& text) const {
    parse_value(text, *m_store);
  }

  void parse() const { parse_value(m_default_value, *m_store); }

  bool has_arg() const { return value_has_arg<T>::value; }

  bool has_default() const { return m_default; }

  virtual std::shared_ptr<Value> default_value(const std::string& value) {
    m_default = true;
    m_default_value = value;
    return shared_from_this();
  }

  std::string get_default_value() const { return m_default_value; }

  const T& get() const {
    if (m_store == nullptr) {
      return *m_result;
    } else {
      return *m_store;
    }
  }

 protected:
  // TODO(iserna): Why a shred ptr?
  std::shared_ptr<T> m_result;
  T* m_store;
  bool m_default = false;
  std::string m_default_value;
};

}

template <typename T>
std::shared_ptr<Value> value() {
  return std::make_shared<values::standard_value<T>>();
}

template <typename T>
std::shared_ptr<Value> value(T& t) {
  return std::make_shared<values::standard_value<T>>(&t);
}

class OptionDetails {
 public:
  OptionDetails(std::string description, std::shared_ptr<const Value> value)
      : m_desc(std::move(description)), m_value(std::move(value)), m_count(0) {}

  const std::string& description() const { return m_desc; }

  bool has_arg() const { return m_value->has_arg(); }

  void parse(const std::string& text) {
    m_value->parse(text);
    ++m_count;
  }

  void parse_default() {
    m_value->parse();
    ++m_count;
  }

  int count() const { return m_count; }

  const Value& value() const { return *m_value; }

  template <typename T>
  const T& as() const {
    return dynamic_cast<const values::standard_value<T>&>(*m_value).get();
  }

 private:
  std::string m_desc;
  std::shared_ptr<const Value> m_value;
  int m_count;
};

class Options {
 public:
  Options() {}

  inline void parse(int& argc, char**& argv);

  inline Options& add_option(const std::string& s, const std::string& l, 
                             std::string desc,
                             std::shared_ptr<const Value> value);

  int count(const std::string& o) const {
    auto iter = m_options.find(o);
    if (iter == m_options.end()) {
      return 0;
    }
    return iter->second->count();
  }

  const OptionDetails& operator[](const std::string& option) const {
    auto iter = m_options.find(option);
    if (iter == m_options.end()) {
      throw option_not_present_exception(option);
    }
    return *iter->second;
  }

 private:
  inline void add_one_option(const std::string& option,
                             std::shared_ptr<OptionDetails> details);

  inline void add_to_option(const std::string& option, const std::string& arg);

  inline void parse_option(std::shared_ptr<OptionDetails> value,
                           const std::string& name,
                           const std::string& arg = "");

  inline void checked_parse_arg(int argc, char* argv[], int& current,
                                std::shared_ptr<OptionDetails> value,
                                const std::string& name);

  std::map<std::string, std::shared_ptr<OptionDetails>> m_options;
};


void Options::parse_option(std::shared_ptr<OptionDetails> value,
                           const std::string& /*name*/,
                           const std::string& arg) {
  value->parse(arg);
}

void Options::checked_parse_arg(int argc, char* argv[], int& current,
                                std::shared_ptr<OptionDetails> value,
                                const std::string& name) {
  if (current + 1 >= argc) {
    throw missing_argument_exception(name);
  } else {
    parse_option(value, name, argv[current + 1]);
    ++current;
  }
}

void Options::add_to_option(const std::string& option, const std::string& arg) {
  auto iter = m_options.find(option);

  if (iter == m_options.end()) {
    throw option_not_exists_exception(option);
  }

  parse_option(iter->second, option, arg);
}

void Options::parse(int& argc, char**& argv) {
  int current = 1;

  int nextKeep = 1;

  while (current != argc) {
    static std::regex option_matcher("--([a-zA-Z0-9][-a-zA-Z0-9]+)(=(.)*)?|-([a-zA-Z])");
    std::match_results<const char*> result;
    std::regex_match(argv[current], result, option_matcher);
 
    if (result.empty()) {
      // not a flag
      argv[nextKeep] = argv[current];
      ++nextKeep;
    } else {
      // short or long option?
      if (result[4].length() != 0) {
        const std::string& s = result[4];

        auto iter = m_options.find(s);

        if (iter == m_options.end()) {
          throw option_not_exists_exception(s);
        }

        auto value = iter->second;

        // if no argument then just add it
        if (!value->has_arg()) {
          parse_option(value, s);
        } else {
          checked_parse_arg(argc, argv, current, value, s);
        }
      } else if (result[1].length() != 0) {
        const std::string& name = result[1];

        auto iter = m_options.find(name);

        if (iter == m_options.end()) {
          throw option_not_exists_exception(name);
        }

        auto opt = iter->second;

        // equals provided for long option?
        if (result[3].length() != 0) {
          // parse the option given

          // but if it doesn't take an argument, this is an error
          if (!opt->has_arg()) {
            throw option_not_has_argument_exception(name, result[3]);
          }

          parse_option(opt, name, result[3]);
        } else {
          if (opt->has_arg()) {
            // parse the next argument
            checked_parse_arg(argc, argv, current, opt, name);
          } else {
            // parse with empty argument
            parse_option(opt, name);
          }
        }
      }
    }

    ++current;
  }

  for (auto& opt : m_options) {
    auto& detail = opt.second;
    auto& value = detail->value();

    if (!detail->count() && value.has_default()) {
      detail->parse_default();
    }
  }

  argc = nextKeep;
}

Options& Options::add_option(const std::string& s,
                         const std::string& l, std::string desc,
                         std::shared_ptr<const Value> value) {
  auto option = std::make_shared<OptionDetails>(std::move(desc), value);

  if (s.size() > 0) {
    add_one_option(s, option);
  }

  if (l.size() > 0) {
    add_one_option(l, option);
  }
  return *this;
}

void Options::add_one_option(const std::string& option,
                             std::shared_ptr<OptionDetails> details) {
  auto in = m_options.emplace(option, details);
  if (!in.second) {
    throw option_exists_error(option);
  }
}
}

#endif  // CXX_OPTS_HPP

