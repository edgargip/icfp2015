// -*- mode: c++ -*-

#ifndef _OBJS_TEST_UTILS_H_
#define _OBJS_TEST_UTILS_H_

#include <cassert>
#include <cstdlib>
#include <iostream>
#include <string>

namespace test {

template <typename T>
std::string ExpressionRepresentation(const T &value, const std::string &expr) {
  std::string value_as_string = std::to_string(value);
  if (value_as_string != expr) {
    value_as_string = expr + " (" + value_as_string + ")";
  }
  return value_as_string;
}

void AssertCell(const Cell &cell, int x, int y,
                const std::string &cell_expr, const std::string &x_expr,
                const std::string &y_expr,
                bool fatal, const std::string &file, int line) {
  if (cell.x != x) {
    std::cerr << file << ":" << line << ":Expected "
              << ExpressionRepresentation(x, x_expr) << " but (" << cell_expr
              << ").x is " << cell.x << std::endl;
    if (fatal) std::abort();
  }
  if (cell.y != y) {
    std::cerr << file << ":" << line << ":Expected "
              << ExpressionRepresentation(y, y_expr) << " but (" << cell_expr
              << ").y is " << cell.y << std::endl;
    if (fatal) std::abort();
  }
}

void AssertPair(const std::pair<int, int> &pair, int first, int second,
                const std::string &pair_expr, const std::string &first_expr,
                const std::string &second_expr,
                bool fatal, const std::string &file, int line) {
  if (pair.first != first) {
    std::cerr << file << ":" << line << ":Expected "
              << ExpressionRepresentation(first, first_expr) << " but ("
              << pair_expr << ").first is " << pair.first << std::endl;
    if (fatal) std::abort();
  }
  if (pair.second != second) {
    std::cerr << file << ":" << line << ":Expected "
              << ExpressionRepresentation(second, second_expr) << " but ("
              << pair_expr << ").second is " << pair.second << std::endl;
    if (fatal) std::abort();
  }
}

}  // namespace test

#define ASSERT_CELL(cell, x, y)                                \
  ::test::AssertCell(cell, x, y, #cell, #x, #y,                \
                     true, __FILE__, __LINE__)

#define EXPECT_CELL(cell, x, y)                                \
  ::test::AssertCell(cell, x, y, #cell, #x, #y,                \
                     false, __FILE__, __LINE__)

#define ASSERT_PAIR(pair, first, second)                                \
  ::test::AssertPair(pair, first, second, #pair, #first, #second,       \
                     true, __FILE__, __LINE__)

#define EXPECT_PAIR(pair, first, second)                                \
  ::test::AssertPair(pair, first, second, #pair, #first, #second,       \
                     false, __FILE__, __LINE__)

#define ASSERT_SINGLETON_UNIT(unit, x, y)                               \
  do {                                                                  \
    assert(unit.Size() == 1);                                           \
    ::test::AssertCell(unit[0], x, y, #unit "[0]", #x, #y,              \
                       true, __FILE__, __LINE__);                       \
  } while (false)

#define EXPECT_SINGLETON_UNIT(unit, x, y)                               \
  do {                                                                  \
    assert(unit.Size() == 1);                                           \
    ::test::AssertCell(unit[0], x, y, #unit "[0]", #x, #y,              \
                       false, __FILE__, __LINE__);                      \
  } while (false)

#endif  // _OBJS_TEST_UTILS_H_
