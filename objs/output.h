// -*- mode: c++ -*-

#ifndef _OBJS_OUTPUT_H_
#define _OBJS_OUTPUT_H_

#include "../json/json11.h"

struct OutputPerSeed {
  int problem_id;
  int seed;
  std::string tag;
  std::string solution;
  
  json11::Json to_json() {
    return json11::Json::object{ {"problemId", problem_id}, {"seed", seed}, {"tag", tag}, {"solution", solution} };
  }
  
  static OutputPerSeed from_json(const json11::Json& json) {
    return { json["problemId"].as_int(), json["seed"].as_int(), json["tag"].as_string(), json["solution"].as_string()};
  }
};

struct Output {
  std::vector<OutputPerSeed> outputs;
  json11::Json to_json() {
    return json11::Json::array(outputs.begin(), outputs.end());
  }
  
  static Output from_json(const json11::Json& json) {
    std::vector<OutputPerSeed> outputs;
    for (auto& e : json.as_array()) {
      outputs.push_back(OutputPerSeed::from_json(e));
    }
    return Output{std::move(outputs)};  
  }
};

#endif  // _OBJS_OUTPUT_H_
