// -*- mode: c++ -*-

#ifndef _OBJS_GAME_H_
#define _OBJS_GAME_H_

#include "../json/json11.h"
#include "../tools/rand.h"
#include "../tools/scorer.h"

#include <cassert>
#include <vector>
#include <tuple>
#include <array>
#include <unordered_set>

enum struct Move {
  E = 0, W = 1, SE = 2, SW = 3, CW = 4, CCW = 5,
  S = 6  // for completeness
};

inline const std::string& GetRepresentation(Move move) {
  static const std::string repr[] = {"E", "W", "SE", "SW", "CW", "CCW", "S"};
  return repr[static_cast<int>(move)];
}

inline const std::vector<Move>& ValidMoves() {
  static const std::vector<Move> valid_moves = { Move::E, Move::W, Move::SE, Move::SW, Move::CW, Move::CCW};
  return valid_moves;
}

inline const std::vector<Move>& ValidMovesWithoutRotations() {
  static const std::vector<Move> valid_moves = { Move::E, Move::W, Move::SE, Move::SW};
  return valid_moves;
}

constexpr int kNumMoves = 6;

const std::map<Move, std::vector<char>>& GetMoveEncodings();

const std::map<char, Move>& GetInverseEncoding();

struct Board;

struct Cell {
  int x;
  int y;

  bool In(const Board& board) const;
  bool Filled(const Board& board) const;  

  json11::Json to_json() const { return json11::Json::object{{"x", x}, {"y", y}}; }
  static Cell from_json(const json11::Json& json) {
    return { json["x"].as_int(), json["y"].as_int()};
  }
};

class Unit {
public:
  Unit() = default;

  // Converts the unit to a JSON representation.
  json11::Json to_json() const;

  // Constructs a unit from a JSON representation.
  static Unit from_json(const json11::Json& json);

  // Constructs a singleton unit from a cell (intended for testing).
  static Unit Singleton(Cell cell);

  // Constructs a singleton unit from a cell and a pivot (intended for testing).
  static Unit Singleton(Cell cell, Cell pivot);

  // Returns a hash for the unit.
  std::size_t Hash() const;

  // Returns the number of cells.
  int Size() const { return members.size(); }

  // Returns the i-th cell.
  const Cell &operator[](int i) const { return members[i]; }

  // Dumps the Unit contents.
  std::string Dump() const;

  // Locates the unit in a starting position for the board.
  Unit StartingUnit(const Board &board) const;

  // Applies the move.
  Unit Apply(Move move) const;

  // Returns true if the unit is in an invalid (locked) position in the given
  // board.
  bool IsLocked(const Board &board) const;

  bool IsEmpty() const { return members.empty(); }

  // Decomposes the movement from 'from' to 'to' as steps in the W->E (X)
  // direction followed by steps in the NW->SE direction.
  static std::pair<int, int> DecomposeWENwSe(const Cell &from, const Cell &to);

  // Recomposes the cell resuting of moving from 'from' in the W->E, NW->SE and
  // NE->SW directions.
  static Cell Recompose(const Cell &pivot, int w_e_vector,
                        int nw_se_vector, int ne_sw_vector);

private:
  Unit(std::vector<Cell> members, Cell pivot)
    : members(std::move(members)), pivot(std::move(pivot)) {
    std::sort(this->members.begin(), this->members.end(), [](const auto& a, const auto&b){
      if (a.x != b.x) return a.x < b.x;
      return a.y < b.y;
    });  
  }

  // Returns div and mod with a positive mod.
  static std::pair<int, int> DivMod(int a, int b);

public:
  std::vector<Cell> members;
  Cell pivot;
};

struct Board {
  friend struct Unit;

  enum CellStatus {
    kEmpty = 0,
    kFilled = 1,
    kActiveUnit = 2,
    kActivePivot = 4,
  };

  Board() = default;

  Board(int width, int height,
        const std::vector<Cell>& filled_cells,
        const std::vector<Unit>& source_template,
        int source_lenght,
        int seed)
    : width_(width),
      height_(height),
      board_(height_, std::vector<int>(width_, kEmpty)),
      source_length_(source_lenght),
      source_template_(&source_template),
      r_(GetRand(seed)) {
    for (auto& cell : filled_cells) {
      board_[cell.y][cell.x] |= kFilled;
    }
    UpdateNextUnit();
  }

  int Width() const { return width_; }
  int Height() const { return height_; }
  auto IsFilled(int x, int y) const { return board_[y][x] & kFilled; }
  auto IsFilled(const Cell& cell) const { return board_[cell.y][cell.x] & kFilled; }

  std::string Dump() const;

  bool IsEmpty() const {
    return width_ == 0 && height_ == 0;
  }

  bool IsFinished() const {
    return finished_ || IsEmpty();
  }

  bool IsLocked() const {
    return locked_;
  }
  
  int GetPerimeter() const;
  int GetHole() const;  

  std::tuple<bool, bool> WillLockOrError(Move move) const;

  Board Apply(Move move) const & {
    return GetApplyFrom(*this, move);
  }

  Board Apply(Move move) && {
    return GetApplyFrom(std::move(*this), move);
  }

  auto Unlock() const & {
    return GetUnlockedFrom(*this);
  }

  auto Unlock() && {
    return GetUnlockedFrom(std::move(*this));
  }

  auto& ActiveUnit() const {
    return active_unit_;
  }

 private:
  void UpdateNextUnit();

  Unit PeekNextUnit() const;

  Unit GetNextUnit();

  static Board GetApplyFrom(Board new_board, Move move);

  static std::tuple<Board, int> GetUnlockedFrom(Board new_board);

 private:
  int width_ = 0;
  int height_ = 0;
  int current_index_ = 0;
  std::vector<std::vector<int>> board_;
  int source_length_ = 0;
  const std::vector<Unit>* source_template_ = nullptr;
  decltype(GetRand(0)) r_ = GetRand(0);
  decltype(MoveScorer()) scorer_ = MoveScorer();
  Unit active_unit_;
  std::unordered_set<size_t> seen_active_unit_hashes_;
  bool finished_ = false;
  bool locked_ = false;
};

inline bool Cell::In(const Board& board) const {
  return x >= 0 && x < board.Width() && y >= 0 && y < board.Height();
}

struct Game {
  int id;
  std::vector<Unit> units;
  int width;
  int height;
  std::vector<Cell> filled;
  int source_length;
  std::vector<int> source_seeds;

  int NumBoards() const {
    return source_seeds.size();
  }

  Board GetInitialBoard(int i) const {
    assert(i >= 0);
    assert(i < source_seeds.size());
    return Board(width, height, filled, units, source_length, source_seeds[i]);
  }

  json11::Json to_json() const { return json11::Json::object{
    {"id", id},
    {"units", json11::Json(units)},
    {"width", width},
    {"height", height},
    {"filled", json11::Json(filled)},
    {"sourceLength", source_length},
    {"sourceSeeds", json11::Json(source_seeds)}}; }


  static Game from_json(const json11::Json& json) {
    std::vector<Unit> units;
    std::vector<Cell> filled;
    std::vector<int> source_seeds;
    for (auto& c : json["units"].as_array())
       units.push_back(Unit::from_json(c));
    for (auto& c : json["filled"].as_array())
       filled.push_back(Cell::from_json(c));
    for (auto& c : json["sourceSeeds"].as_array())
       source_seeds.push_back(c.as_int());
    return { json["id"].as_int(), std::move(units), json["width"].as_int(), json["height"].as_int(), std::move(filled), json["sourceLength"].as_int(), std::move(source_seeds)};
  }
};

namespace std {

template <>
struct hash<Unit> {
  typedef Unit argument_type;
  typedef std::size_t result_type;
  result_type operator()(const argument_type &unit) const {
    return unit.Hash();
  }
};

}  // namespace std

#endif // _OBJS_GAME_H_
