#include "game.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <sstream>

json11::Json Unit::to_json() const {
  return json11::Json::object{
    {"members", json11::Json(members)},
    {"pivot", pivot}
  };
}

/* static */ Unit Unit::from_json(const json11::Json& json) {
  std::vector<Cell> members;
  for (auto& c : json["members"].as_array())
    members.push_back(Cell::from_json(c));
  return {std::move(members), Cell::from_json(json["pivot"])};
}

/* static */ Unit Unit::Singleton(Cell cell) {
  std::vector<Cell> members(1, cell);
  return {std::move(members), std::move(cell)};
}

/* static */ Unit Unit::Singleton(Cell cell, Cell pivot) {
  std::vector<Cell> members(1, std::move(cell));
  return {std::move(members), std::move(pivot)};
}

std::size_t Unit::Hash() const {
  auto int_hash = [] (unsigned x) {
    x = ((x >> 16) ^ x) * 0x45d9f3bU;
    x = ((x >> 16) ^ x) * 0x45d9f3bU;
    x = ((x >> 16) ^ x);
    return x;
  };
  auto combine_hash = [] (unsigned x, unsigned y) {
    unsigned hash = 5381U;  // Bernstein Hash.
    hash = hash * 33U + x;
    hash = hash * 33U + y;
    return hash;
  };
  unsigned result = 0;
  for (const auto &member : members) {
    result = combine_hash(result, int_hash(member.x));
    result = combine_hash(result, int_hash(member.y));
  }
  result = combine_hash(result, int_hash(pivot.x));
  result = combine_hash(result, int_hash(pivot.y));  
  return result;
}

std::string Unit::Dump() const {
  if (Size() == 0) return "{}";
  std::ostringstream oss;
  oss << "{(" << members[0].x << "," << members[0].y << ")";
  std::for_each(members.begin() + 1, members.end(), [&oss](const auto& member) {
    oss << ",(" << member.x << "," << member.y << ")";
  });
  oss << "/(" << pivot.x << "," << pivot.y << ")}";
  return oss.str();
}

Unit Unit::StartingUnit(const Board &board) const {
  // Find minimal 'y' coordinate, and 'x' range.
  auto it = members.begin();
  assert(it != members.end());
  int min_y = it->y;
  int min_x = it->x;
  int max_x = it->x;
  for (++it; it != members.end(); ++it) {
    if (it->y < min_y) min_y = it->y;
    if (it->x < min_x) min_x = it->x;
    else if (it->x > max_x) max_x = it->x;
  }

  // Center in board.
  int x_size = max_x - min_x + 1;
  int x_west_padding = (board.Width() - x_size) / 2;
  int x_shift = x_west_padding - min_x;

  // Mapping function.
  auto map = [x_shift, min_y] (const Cell &cell) {
    return Cell{cell.x + x_shift, cell.y - min_y};
  };

  // Transform.
  std::vector<Cell> new_members;
  new_members.reserve(members.size());
  std::transform(members.begin(), members.end(),
                 std::back_inserter(new_members), map);

  // Check
  {
    auto it2 = new_members.begin();
    int leftmost_x = it2->x;
    int rightmost_x = it2->x;
    for (++it2; it2 != new_members.end(); ++it2) {
      if (it2->x < leftmost_x) leftmost_x = it2->x;
      else if (it2->x > rightmost_x) rightmost_x = it2->x;
    }
    int right_margin = board.Width() - 1 - rightmost_x;
    if (leftmost_x != right_margin && leftmost_x != right_margin - 1) {
      std::cerr << "XXXX " << Dump() << " "
                << Unit{new_members, map(pivot)}.Dump() << " "
                << board.Width() << " " << min_x << " " << max_x
                << " " << leftmost_x << " " << rightmost_x << " "
                << right_margin << std::endl;
      assert(false);
    }
  }

  return Unit{std::move(new_members), map(pivot)};
}

Unit Unit::Apply(Move move) const {
  std::function<Cell (const Cell &)> transform;
  switch (move) {
  case Move::E:
    transform = [](const Cell &cell) { return Cell{cell.x + 1, cell.y}; };
    break;
  case Move::W:
    transform = [](const Cell &cell) { return Cell{cell.x - 1, cell.y}; };
    break;
  case Move::SE:
    transform = [](const Cell &cell) {
      if (cell.y % 2 == 0) {
        return Cell{cell.x, cell.y + 1};
      } else {
        return Cell{cell.x + 1, cell.y + 1};
      }
    };
    break;
  case Move::SW:
    transform = [](const Cell &cell) {
      if (cell.y % 2 == 0) {
        return Cell{cell.x - 1, cell.y + 1};
      } else {
        return Cell{cell.x, cell.y + 1};
      }
    };
    break;
  case Move::CW:
    transform = [this](const Cell &cell) {
      // Decompose in the W->E and NW->SE directions.
      int w_e_vector, nw_se_vector;
      std::tie(w_e_vector, nw_se_vector) = DecomposeWENwSe(pivot, cell);

      // W->E becomes NW->SE, NW->SE becomes NE->SW.
      return Recompose(pivot, /* W->E */ 0, /* NW->SE */ w_e_vector,
                       /* NE->SW */ nw_se_vector);
    };
    break;
  case Move::CCW:
    transform = [this](const Cell &cell) {
      // Decompose in the W->E and NW->SE directions.
      int w_e_vector, nw_se_vector;
      std::tie(w_e_vector, nw_se_vector) = DecomposeWENwSe(pivot, cell);

      // W->E becomes -NE->SW, NW->SE becomes W->E.
      return Recompose(pivot, /* W->E */ nw_se_vector, /* NW->SE */ 0,
                       /* NE->SW */ -w_e_vector);
    };
    break;
  default:
    assert(false);
  }
  std::vector<Cell> new_members;
  new_members.reserve(members.size());
  std::transform(members.begin(), members.end(),
                 std::back_inserter(new_members), transform);

  return Unit{std::move(new_members), transform(pivot)};
}

bool Unit::IsLocked(const Board &board) const {
  for (const auto &member : members) {
    if (!member.In(board) || board.IsFilled(member.x, member.y)) {
      return true;
    }
  }
  return false;
}

/* static */ std::pair<int, int>
Unit::DecomposeWENwSe(const Cell &from, const Cell &to) {
  int nw_se_vector = to.y - from.y;
  int nw_se_vector_div_2, nw_se_vector_mod_2;
  std::tie(nw_se_vector_div_2, nw_se_vector_mod_2) = DivMod(nw_se_vector, 2);

  const int x_in_to_row = from.x + nw_se_vector_div_2
    + (from.y % 2 == 1 && nw_se_vector_mod_2 == 1 ? 1 : 0);

  return {to.x - x_in_to_row, nw_se_vector};
}

/* static */ Cell
Unit::Recompose(const Cell &pivot, int w_e_vector,
                int nw_se_vector, int ne_sw_vector) {
  int new_x = pivot.x;
  int new_y = pivot.y;

  int nw_se_vector_div_2, nw_se_vector_mod_2;
  std::tie(nw_se_vector_div_2, nw_se_vector_mod_2) = DivMod(nw_se_vector, 2);
  new_x += nw_se_vector_div_2
    + (new_y % 2 == 1 && nw_se_vector_mod_2 == 1 ? 1 : 0);
  new_y += nw_se_vector;

  int ne_sw_vector_div_2, ne_sw_vector_mod_2;
  std::tie(ne_sw_vector_div_2, ne_sw_vector_mod_2) = DivMod(ne_sw_vector, 2);
  new_x -= ne_sw_vector_div_2
    + (new_y % 2 == 0 && ne_sw_vector_mod_2 == 1 ? 1 : 0);
  new_y += ne_sw_vector;

  return Cell{new_x + w_e_vector, new_y};
}

/* static */ std::pair<int, int> Unit::DivMod(int a, int b) {
  int div = a / b;
  int mod = a % b;
  if (mod < 0) {
    div -= 1;
    mod += b;
  }
  return {div, mod};
}
