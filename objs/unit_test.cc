#include <cassert>
#include <iostream>
#include <string>

#include "game.h"
#include "test_utils.h"

void TestMovesFromEven() {
  Unit unit = Unit::Singleton({3,0});

  EXPECT_SINGLETON_UNIT(unit.Apply(Move::E), 4, 0);
  EXPECT_SINGLETON_UNIT(unit.Apply(Move::W), 2, 0);
  EXPECT_SINGLETON_UNIT(unit.Apply(Move::SE), 3, 1);
  EXPECT_SINGLETON_UNIT(unit.Apply(Move::SW), 2, 1);
}

void TestMovesFromOdd() {
  Unit unit = Unit::Singleton({3,1});

  EXPECT_SINGLETON_UNIT(unit.Apply(Move::E), 4, 1);
  EXPECT_SINGLETON_UNIT(unit.Apply(Move::W), 2, 1);
  EXPECT_SINGLETON_UNIT(unit.Apply(Move::SE), 4, 2);
  EXPECT_SINGLETON_UNIT(unit.Apply(Move::SW), 3, 2);
}

void TestDecomposeOdd() {
  Cell pivot{1,3};

  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{0, 1}), 0, -2);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{1, 1}), 1, -2);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{2, 1}), 2, -2);

  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{0, 2}), -1, -1);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{1, 2}), 0, -1);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{2, 2}), 1, -1);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{3, 2}), 2, -1);

  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{0, 3}), -1, 0);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{1, 3}), 0, 0);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{2, 3}), 1, 0);

  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{0, 4}), -2, 1);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{1, 4}), -1, 1);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{2, 4}), 0, 1);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{3, 4}), 1, 1);

  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{0, 5}), -2, 2);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{1, 5}), -1, 2);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{2, 5}), 0, 2);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{3, 5}), 1, 2);
}

void TestDecomposeEven() {
  Cell pivot{2,4};

  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{0, 2}), -1, -2);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{1, 2}), 0, -2);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{2, 2}), 1, -2);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{3, 2}), 2, -2);

  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{0, 3}), -1, -1);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{1, 3}), 0, -1);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{2, 3}), 1, -1);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{3, 3}), 2, -1);

  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{0, 4}), -2, 0);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{1, 4}), -1, 0);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{2, 4}), 0, 0);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{3, 4}), 1, 0);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{4, 4}), 2, 0);

  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{0, 5}), -2, 1);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{1, 5}), -1, 1);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{2, 5}), 0, 1);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{3, 5}), 1, 1);

  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{0, 6}), -3, 2);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{1, 6}), -2, 2);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{2, 6}), -1, 2);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{3, 6}), 0, 2);
  EXPECT_PAIR(Unit::DecomposeWENwSe(pivot, Cell{4, 6}), 1, 2);
}

void TestRecomposeOdd() {
  Cell pivot{3,3};

  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ 0, /* NE->SW */ 0),
      3, 3);

  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ -2, /* NW->SE */ 0, /* NE->SW */ 0),
      1, 3);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ -1, /* NW->SE */ 0, /* NE->SW */ 0),
      2, 3);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 1, /* NW->SE */ 0, /* NE->SW */ 0),
      4, 3);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 2, /* NW->SE */ 0, /* NE->SW */ 0),
      5, 3);

  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ -2, /* NE->SW */ 0),
      2, 1);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ -1, /* NE->SW */ 0),
      3, 2);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ 1, /* NE->SW */ 0),
      4, 4);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ 2, /* NE->SW */ 0),
      4, 5);

  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ 0, /* NE->SW */ -2),
      4, 1);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ 0, /* NE->SW */ -1),
      4, 2);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ 0, /* NE->SW */ 1),
      3, 4);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ 0, /* NE->SW */ 2),
      2, 5);

  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 1, /* NW->SE */ 1, /* NE->SW */ 1),
      4, 5);
}

void TestRecomposeEven() {
  Cell pivot{3,4};

  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ 0, /* NE->SW */ 0),
      3, 4);

  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ -2, /* NW->SE */ 0, /* NE->SW */ 0),
      1, 4);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ -1, /* NW->SE */ 0, /* NE->SW */ 0),
      2, 4);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 1, /* NW->SE */ 0, /* NE->SW */ 0),
      4, 4);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 2, /* NW->SE */ 0, /* NE->SW */ 0),
      5, 4);

  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ -2, /* NE->SW */ 0),
      2, 2);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ -1, /* NE->SW */ 0),
      2, 3);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ 1, /* NE->SW */ 0),
      3, 5);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ 2, /* NE->SW */ 0),
      4, 6);

  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ 0, /* NE->SW */ -2),
      4, 2);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ 0, /* NE->SW */ -1),
      3, 3);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ 0, /* NE->SW */ 1),
      2, 5);
  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 0, /* NW->SE */ 0, /* NE->SW */ 2),
      2, 6);

  EXPECT_CELL(
      Unit::Recompose(pivot, /* W->E */ 1, /* NW->SE */ 1, /* NE->SW */ 1),
      4, 6);
}

void TestClockwiseRotationsAtOne() {
  Unit unit = Unit::Singleton({1,1}, {2,2});

  EXPECT_SINGLETON_UNIT(unit.Apply(Move::CW), 2, 1);
  EXPECT_SINGLETON_UNIT(unit.Apply(Move::CW).Apply(Move::CW), 3, 2);
  EXPECT_SINGLETON_UNIT(unit
                        .Apply(Move::CW).Apply(Move::CW).Apply(Move::CW), 2, 3);
  EXPECT_SINGLETON_UNIT(unit
                        .Apply(Move::CW).Apply(Move::CW).Apply(Move::CW)
                        .Apply(Move::CW), 1, 3);
  EXPECT_SINGLETON_UNIT(unit
                        .Apply(Move::CW).Apply(Move::CW).Apply(Move::CW)
                        .Apply(Move::CW).Apply(Move::CW), 1, 2);
  EXPECT_SINGLETON_UNIT(unit
                        .Apply(Move::CW).Apply(Move::CW).Apply(Move::CW)
                        .Apply(Move::CW).Apply(Move::CW).Apply(Move::CW), 1, 1);
}

void TestCounterClockwiseRotationsAtOne() {
  Unit unit = Unit::Singleton({1,1}, {2,2});

  EXPECT_SINGLETON_UNIT(unit.Apply(Move::CCW), 1, 2);
  EXPECT_SINGLETON_UNIT(unit.Apply(Move::CCW).Apply(Move::CCW), 1, 3);
  EXPECT_SINGLETON_UNIT(unit
                        .Apply(Move::CCW).Apply(Move::CCW).Apply(Move::CCW),
                        2, 3);
  EXPECT_SINGLETON_UNIT(unit
                        .Apply(Move::CCW).Apply(Move::CCW).Apply(Move::CCW)
                        .Apply(Move::CCW), 3, 2);
  EXPECT_SINGLETON_UNIT(unit
                        .Apply(Move::CCW).Apply(Move::CCW).Apply(Move::CCW)
                        .Apply(Move::CCW).Apply(Move::CCW), 2, 1);
  EXPECT_SINGLETON_UNIT(unit
                        .Apply(Move::CCW).Apply(Move::CCW).Apply(Move::CCW)
                        .Apply(Move::CCW).Apply(Move::CCW).Apply(Move::CCW),
                        1, 1);
}

int main(int argc, char *argv[]) {
  TestMovesFromEven();
  TestMovesFromOdd();
  TestDecomposeOdd();
  TestDecomposeEven();
  TestRecomposeOdd();
  TestRecomposeEven();
  TestClockwiseRotationsAtOne();
  TestCounterClockwiseRotationsAtOne();
  return 0;
}
