#include "game.h"

#include <iostream>
#include <string>
#include <random>

int main() {
  std::string err;
  auto s = json11::Json::parse(R"(
    {
      "id": 1,
      "units": [ {"members": [{"x":0, "y":0}], "pivot": {"x":0, "y":0}},
                 {"members": [{"x":0, "y":0}], "pivot": {"x":0, "y":0}}],
      "width": 5,
      "height": 5,
      "filled": [{"x":0, "y":0}],
      "sourceLength": 7,
      "sourceSeeds": [1, 2, 3]
    }
  )", err);
  auto game = Game::from_json(s);
  std::cout << game.to_json().dump() << std::endl;

  auto board = game.GetInitialBoard(0);
  std::cout << board.Dump() << std::endl;
  auto score = 0;
  std::mt19937 gen(42);
  std::uniform_int_distribution<> dis(0, 1);
  while (!board.IsFinished()) {
    board = std::move(board).Apply(dis(gen) > 0 ? Move::SW : Move::SE);
    if (board.IsEmpty()) {
      std::cout << "Erroneous move!" << std::endl;
    } else {
      int round;
      std::tie(board, round) = std::move(board).Unlock();
      score += round;
    }
    std::cout << board.Dump() << std::endl;
  }
  std::cout << "Score: " << score << std::endl;
  return 0;
}
