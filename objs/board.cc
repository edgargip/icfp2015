#include "game.h"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <set>

int Board::GetPerimeter() const {
  assert(!IsLocked());
  int perimeter = 0;
  for (int y = 0; y < height_; ++y) {
    for (int x = 0; x < width_; ++x) {
      if (board_[y][x] == kFilled) {
        // Possible neigh
        auto a = y % 2 ? -1 : 0;
        Cell left{x - 1, y}, right{x + 1, y};
        Cell up1{x + a , y -1}, up2{ x + a + 1, y -1};
        Cell down1{ x + a , y +1}, down2{ x + a + 1, y +1};        
        if (left.In(*this) && !IsFilled(left)) ++perimeter;
        if (right.In(*this) && !IsFilled(right)) ++perimeter;
        if (up1.In(*this) && !IsFilled(up1)) ++perimeter;
        if (up2.In(*this) && !IsFilled(up2)) ++perimeter;
        if (down1.In(*this) && !IsFilled(down1)) ++perimeter;
        if (down2.In(*this) && !IsFilled(down2)) ++perimeter;        
      }
    }
  }
  return perimeter;
}

int Board::GetHole() const {
  assert(!IsLocked());
  int hole_coeff = 0;
  int empty = 0;
  for (int y = 0; y < height_; ++y) {
    for (int x = 0; x < width_; ++x) {
      if (board_[y][x] == kEmpty) {
        // Possible neigh
        ++empty;
        auto a = y % 2 ? -1 : 0;
        Cell left{x - 1, y}, right{x + 1, y};
        Cell up1{x + a , y -1}, up2{ x + a + 1, y -1};
        Cell down1{ x + a , y +1}, down2{ x + a + 1, y +1};
        int neigh_posibles = 0;
        int neigh_filled = 0;                
        if (!left.In(*this)) {
          ++neigh_posibles;
          if (!IsFilled(left))
            ++neigh_filled;
        }        
        if (!right.In(*this)) {
          ++neigh_posibles;
          if (!IsFilled(left))
            ++neigh_filled;
        }        
        if (!up1.In(*this)) {
          ++neigh_posibles;
          if (!IsFilled(left))
            ++neigh_filled;
        }        
        if (!up2.In(*this)) {
          ++neigh_posibles;
          if (!IsFilled(left))
            ++neigh_filled;
        }        
        if (!down1.In(*this)) {
          ++neigh_posibles;
          if (!IsFilled(left))
            ++neigh_filled;
        }        
        if (!down2.In(*this)) {
          ++neigh_posibles;
          if (!IsFilled(left))
            ++neigh_filled;
        }
        if (neigh_posibles > 0) {
          hole_coeff += neigh_filled * 1000 / neigh_posibles;      
        } 
      }
    }
  }
  assert(empty > 0);
  return hole_coeff / empty;
}

void Board::UpdateNextUnit() {
  locked_ = false;
  finished_ = false;
  active_unit_ = GetNextUnit();

  if (active_unit_.IsEmpty() || active_unit_.IsLocked(*this)) {
    finished_ = true;
  }
  for (auto& cell : active_unit_.members) {
    if (cell.In(*this)) {
      board_[cell.y][cell.x] |= kActiveUnit;
    }
  }
  auto& pivot = active_unit_.pivot;
  if (pivot.In(*this)) {
    board_[pivot.y][pivot.x] |= kActivePivot;
  }
  seen_active_unit_hashes_ = {active_unit_.Hash()};
}

Unit Board::PeekNextUnit() const {
  if (current_index_ >= source_length_) {
    return Unit{};
  }
  auto nr = r_;
  return source_template_->at(nr() % source_template_->size()).StartingUnit(*this);
}

Unit Board::GetNextUnit() {
  if (current_index_ >= source_length_) {
    return Unit{};
  }
  ++current_index_;
  return source_template_->at(r_() % source_template_->size()).StartingUnit(*this);
}

std::string Board::Dump() const {
  std::string out;
  out += std::string(2 * width_ + 2, '=') + "\n";
  for (int i = 0; i < height_; ++i) {
    out += i % 2 ? " *" : "*";
    for (int j = 0; j < width_; ++j) {
      out += (board_[i][j] == kEmpty) ? "_" : std::to_string(board_[i][j]);
      out += " ";
    }
    out += "\b*\n";
  }
  out += std::string(2 * width_ + 2, '=') + "\n";
  return out;
}

/*static*/ Board Board::GetApplyFrom(Board new_board, Move move) {
  assert(!new_board.IsFinished());
  auto applied_unit = new_board.active_unit_.Apply(move);
  auto applied_unit_hash = applied_unit.Hash();
  if (new_board.seen_active_unit_hashes_.count(applied_unit_hash) > 0) {
    // std::cout << applied_unit_hash << std::endl;
    // Error: Moving back to previous position.
    return Board{};
  }
  // std::cout << "Insert: " << applied_unit_hash << std::endl;  
  new_board.seen_active_unit_hashes_.insert(applied_unit_hash);
  if (applied_unit.IsLocked(new_board)) {
    new_board.locked_ = true;
    return new_board;
  }
  for (auto& cell : new_board.active_unit_.members) {
    assert(cell.In(new_board));
    new_board.board_[cell.y][cell.x] &= ~kActiveUnit;
  }
  auto& old_pivot = new_board.active_unit_.pivot;
  if (old_pivot.In(new_board)) {
    new_board.board_[old_pivot.y][old_pivot.x] &= ~kActivePivot;
  }
  for (auto& cell : applied_unit.members) {
    assert(cell.In(new_board));
    new_board.board_[cell.y][cell.x] |= kActiveUnit;
  }
  auto& new_pivot = applied_unit.pivot;
  if (new_pivot.In(new_board)) {
    new_board.board_[new_pivot.y][new_pivot.x] |= kActivePivot;
  }
  new_board.active_unit_ = std::move(applied_unit);
  return new_board;
}

/*static*/ std::tuple<Board, int> Board::GetUnlockedFrom(Board new_board) {
  int unit_size = new_board.active_unit_.Size();
  assert(!new_board.IsFinished());
  if (!new_board.locked_) {
    return std::make_tuple(std::move(new_board), 0);
  }
  // Change active for filled
  for (auto& cell : new_board.active_unit_.members) {
    assert(cell.In(new_board));
    new_board.board_[cell.y][cell.x] &= ~kActiveUnit;
    new_board.board_[cell.y][cell.x] |= kFilled;
  }
  auto& pivot = new_board.active_unit_.pivot;
  if (pivot.In(new_board)) {
    new_board.board_[pivot.y][pivot.x] &= ~kActivePivot;
  }
  // point scoring
  auto completed_lines = 0;
  // Fuck the standard! Magic!
  auto first = new_board.board_.rbegin();
  auto last = new_board.board_.rend();
  for(auto i = first; i != last; ++i) {
    if (std::any_of(i->begin(), i->end(), [](const auto& v) { return v == kEmpty; })) {
      std::swap(*first, *i);
      // std::cout << "Distance: " << std::distance(i, first) << std::endl;
      first++;
    } else {
      *i = std::vector<int>(new_board.width_, kEmpty);
      ++completed_lines;
    }
  }
  //
  auto move_score = new_board.scorer_(unit_size, completed_lines);
  // get the next unit.
  new_board.UpdateNextUnit();
  return std::make_tuple(std::move(new_board), move_score);
}

std::tuple<bool, bool> Board::WillLockOrError(Move move) const {
  auto applied_unit = active_unit_.Apply(move);
  auto applied_unit_hash = applied_unit.Hash();
  if (seen_active_unit_hashes_.count(applied_unit_hash) > 0) {
    return {false, true};
  }
  if (applied_unit.IsLocked(*this)) {
    return {true, false};
  }
  return {false, false};
}
