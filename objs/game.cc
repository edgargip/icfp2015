#include <map>
#include <vector>

#include "game.h"

const std::map<Move, std::vector<char>>& GetMoveEncodings() {
  static const std::map<Move, std::vector<char>> kMoveEncodings = {
    { Move::E, {'b', 'c', 'e', 'f', 'y', '2'} },
    { Move::W, {'p', '\'', '!', '.', '0', '3'} },
    { Move::SE, {'l', 'm', 'n', 'o', ' ', '5'} },
    { Move::SW, {'a', 'g', 'h', 'i', 'j', '4'} },
    { Move::CW, {'d', 'q', 'r', 'v', 'z', '1'} },
    { Move::CCW, {'k', 's', 't', 'u', 'w', 'x'} },
    { Move::S, {'\n', '\t', '\r'} }
  };
  return kMoveEncodings;
}

const std::map<char, Move>& GetInverseEncoding() {
  static const std::map<char, Move> kInverseEncoding = []() {
    std::map<char, Move> inverse_encoding;
    auto& encoding = GetMoveEncodings();
    for (auto& kv : encoding) {
      for (char encoding : kv.second) {
        inverse_encoding.emplace(encoding, kv.first);
      }
    }
    return inverse_encoding;
  }();
  return kInverseEncoding;
}
