#include <cassert>
#include <iostream>
#include <map>
#include <string>
#include <random>
#include <utility>

#include "../opts/opts.h"
#include "../objs/game.h"
#include "../objs/output.h"
#include "../solver/trie.h"
#include "../tools/reader.h"

using StringTrie = Trie<std::string, std::string>;

int main(int argc, char** argv) {
  cxxopts::Options options;
  options
    .add_option("i", "", "Input", cxxopts::value<std::string>())
    .add_option("o", "", "Output", cxxopts::value<std::string>())
    .add_option("q", "", "Quick Game", cxxopts::value<bool>())
    .add_option("s", "", "Skip Games", cxxopts::value<bool>())
    .add_option("d", "", "Dump Boards", cxxopts::value<bool>())
    .add_option("p", "", "Power", cxxopts::value<std::vector<std::string>>());
  options.parse(argc, argv);

  assert(options.count("i") > 0);
  assert(options.count("o") > 0);

  auto game = ReadFromFile<Game>(options["i"].as<std::string>());
  auto output = ReadFromFile<Output>(options["o"].as<std::string>());
  auto quick = options["q"].as<bool>();
  auto skip = options["s"].as<bool>();
  auto dump = options["d"].as<bool>();
  const auto& power_phrases = options.count("p")
    ? options["p"].as<std::vector<std::string>>()
    : std::vector<std::string>();

  StringTrie phrases_trie;
  for (const std::string &phrase : power_phrases) {
    phrases_trie.Insert(phrase, phrase);
  }

  assert(game.source_seeds.size() == output.outputs.size());
  std::map<int, int> seed_to_score;
  for (int i = 0; i < output.outputs.size(); ++i) {
    assert(game.id == output.outputs[i].problem_id);
    assert(output.outputs[i].seed == game.source_seeds[i]);
    if (!skip) std::cin.get();
    std::cout << "Playing id #" << game.id
              << " Seed: " << game.source_seeds[i] << std::endl;
    std::cout << "Replaying moves" << std::endl;

    Board board = game.GetInitialBoard(i);
    std::cout << board.ActiveUnit().Dump() << std::endl;
    std::cout << board.Dump() << std::endl;
    int score = 0;
    const auto& solution = output.outputs[i].solution;

    int num_c = 0;
    for (char c : solution) {
      const Move next_move = GetInverseEncoding().at(c);
      if (next_move == Move::S) continue;
      std::cout << "Move #" << num_c++ << ": "
                << GetRepresentation(next_move) << std::flush;
      assert(!board.IsFinished());
      board = std::move(board).Apply(next_move);
      int round;
      std::tie(board, round) = std::move(board).Unlock();
      score += round;
      std::cout << " Score: " << score << std::endl;
      if (dump) {
         std::cout << board.ActiveUnit().Dump() << std::endl;
         std::cout << board.Dump() << std::endl;
      }
      if (!quick) std::cin.get();
    }
    assert(board.IsFinished());
    std::cout << "Total move score: " << score << std::endl;

    StringTrie::Matches phrase_matches = phrases_trie.FindAll(solution);
    std::map<std::string, int> phrase_counts;
    for (const auto &match : phrase_matches) {
      ++phrase_counts[std::get<2>(match)->Value()];
    }
    int phrase_score = 0;
    for (const auto &phrase_count : phrase_counts) {
      std::cout << "Phrase " << phrase_count.first << " mentioned "
                << phrase_count.second << " times" << std::endl;
      phrase_score += 300 + 2 * phrase_count.first.size() * phrase_count.second;
    }
    std::cout << "Total phrase score: " << phrase_score << std::endl;
    score += phrase_score;

    std::cout << "Moves: " << solution.size() << std::endl;
    std::cout << "Seed: " << game.source_seeds[i] << std::endl;
    std::cout << "Score: " << score << std::endl;
    seed_to_score[game.source_seeds[i]] = score;
  }

  auto total_score =
       std::accumulate(seed_to_score.begin(), seed_to_score.end(), 0,
                       [](auto v, const auto& kv) { return v + kv.second; });
  std::cout << "Total score: " << total_score << " in " << seed_to_score.size()
            << " boards, so: " << total_score / game.source_seeds.size()
            << std::endl;
  return 0;
}
