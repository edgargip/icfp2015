#include "bt_encoding_optimizer.h"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <memory>

#include "../objs/game.h"
#include "mfset.h"

void BtEncodingOptimizer::
SetPowerPhrases(const std::vector<std::string> power_phrases) {
  phrase_index_.SetPhrases(power_phrases);
}

std::string BtEncodingOptimizer::Encode(const std::vector<Move> &moves) const {
  std::vector<std::pair<int, int>> subobtimization_ranges =
      FindSuboptizimationRanges(moves);

  std::string result;
  for (const auto &range : subobtimization_ranges) {
    result += EncodeRange(moves, range.first, range.second);
  }
  return result;
}

std::vector<std::pair<int, int>> BtEncodingOptimizer::
FindSuboptizimationRanges(const std::vector<Move> &moves) const {
  MFSet mf_set(moves.size());

  std::vector<const PhraseIndex::MoveTrie::Node *> open_nodes = {
      phrase_index_.PhrasesByMoves().Root()
  };
  for (int p = 0; p < moves.size(); ++p) {
    std::vector<const PhraseIndex::MoveTrie::Node *> next_nodes;
    for (auto node : open_nodes) {
      auto next_node = node->Next(moves[p]);
      if (next_node != nullptr) {
        if (next_node->HasValue()) {
          for (int r = p - next_node->Value()->phrase.size() + 1; r < p; ++r) {
            mf_set.Merge(r, p);
          }
        }
        if (next_node->HasChildren()) {
          next_nodes.push_back(next_node);
        }
      }
    }
    next_nodes.push_back(phrase_index_.PhrasesByMoves().Root());
    open_nodes = std::move(next_nodes);
  }

  std::vector<std::vector<int>> clusters = mf_set.Clusters();
  std::vector<std::pair<int, int>> ranges;
  ranges.reserve(clusters.size());
  std::transform(clusters.begin(), clusters.end(), std::back_inserter(ranges),
                 [](const std::vector<int> &cluster) {
                   return std::make_pair(cluster.front(), cluster.back() + 1);
                 });
  std::sort(clusters.begin(), clusters.end());

  int prev_end = 0;
  for (const auto &range : ranges) {
    assert(range.first == prev_end);
    prev_end = range.second;
  }
  assert(prev_end == moves.size());

  return ranges;
}

std::string BtEncodingOptimizer::
EncodeRange(const std::vector<Move> &moves, int start, int end) const {
  if (start + 1 == end) {
    return std::string(1, GetMoveEncodings().at(moves[start]).front());
  }

  BTState bt_state{moves, start, end};
  std::vector<const PhraseIndex::StringTrie::Node *> open_nodes = {
      phrase_index_.PhrasesByName().Root()
  };
  EncodeRangeRec(bt_state, start, 0, open_nodes);
  return bt_state.best_encoding;
}

void BtEncodingOptimizer::
EncodeRangeRec(BTState &bt_state, int current, int score,
               const std::vector<const PhraseIndex::StringTrie::Node *>
                   open_nodes) const {
  if (current == bt_state.end) {
    if (score > bt_state.best_score) {
      bt_state.best_encoding = bt_state.current_encoding;
      bt_state.best_score = score;
    }
    return;
  }

  for (char ch : GetMoveEncodings().at(bt_state.moves[current])) {
    int next_score = score;
    std::vector<const PhraseIndex::StringTrie::Node *> next_nodes;
    for (auto node : open_nodes) {
      auto next_node = node->Next(ch);
      if (next_node != nullptr) {
        if (next_node->HasValue()) {
          int phrase_length = next_node->Value()->phrase.size();
          next_score += 2 * phrase_length;
        }
        if (next_node->HasChildren()) {
          next_nodes.push_back(next_node);
        }
      }
    }
    next_nodes.push_back(phrase_index_.PhrasesByName().Root());

    bt_state.current_encoding.push_back(ch);
    EncodeRangeRec(bt_state, current + 1, next_score, next_nodes);
    bt_state.current_encoding.pop_back();
  }
}
