// -*- mode: c++ -*-

#ifndef _SOLVER_RANDOM_SOLVER_H_
#define _SOLVER_RANDOM_SOLVER_H_

#include <random>
#include <string>
#include <vector>

#include "../objs/game.h"

#include "solver.h"

class RandomMoveSolver : public MoveSolver {
public:
  template <typename RNG>
  RandomMoveSolver(RNG rng);
  
  ~RandomMoveSolver() override {}

  std::vector<Move> Solve(Board board) const override;

private:
  std::function<Move ()> random_move_;
};

class RandomMoveSolverWithoutLocking : public MoveSolver {
 public:
  template <typename RNG>
  RandomMoveSolverWithoutLocking(RNG rng, bool rotations = true) : rotations_(rotations) {
    random_move_ = [rng = std::move(rng)](const std::vector<Move>& v) mutable {
      std::uniform_int_distribution<> dis(0, v.size() - 1);
      return v[dis(rng)];
    };
  }

  ~RandomMoveSolverWithoutLocking() override {}

  std::vector<Move> Solve(Board board) const override;
 private:
  const bool rotations_;
  std::function<Move (const std::vector<Move>& v)> random_move_;
};

class RandomDummySolver : public MoveSolver {
 public:
  template <typename RNG>
  RandomDummySolver(RNG rng) {
    std::uniform_int_distribution<> dis(0, 1);
    random_move_ = [rng = std::move(rng), dis = std::move(dis)]() mutable {
      return Move::SE;
    };
  }
  
  ~RandomDummySolver() override {}  

  std::vector<Move> Solve(Board board) const override;
 private:
  std::function<Move ()> random_move_;
};

class RandomEncodingOptimizer : public EncodingOptimizer {
public:
  template <typename RNG>
  RandomEncodingOptimizer(RNG rng);
  
  ~RandomEncodingOptimizer() override {}

  void SetPowerPhrases(std::vector<std::string> power_phrases) override {}

  std::string Encode(const std::vector<Move> &moves) const override;

private:
  std::function<int (Move)> random_encoding_;
};

class DummyEncodingOptimizer : public EncodingOptimizer {
public:

  ~DummyEncodingOptimizer() override {}
  
  void SetPowerPhrases(std::vector<std::string> power_phrases) override {}

  std::string Encode(const std::vector<Move> &moves) const override;
};

template <typename RNG>
RandomMoveSolver::RandomMoveSolver(RNG rng) {
  std::uniform_int_distribution<> dis(0, kNumMoves - 1);
  random_move_ = [dis = std::move(dis), rng = std::move(rng)]() mutable {
    return Move(dis(rng));
  };
}

template <typename RNG>
RandomEncodingOptimizer::RandomEncodingOptimizer(RNG rng) {
  random_encoding_ = [rng = std::move(rng)](Move move) mutable {
    const auto &encodings = GetMoveEncodings().at(move);
    std::uniform_int_distribution<> dis(0, encodings.size() - 1);
    return encodings[dis(rng)];
  };
}

#endif  // _SOLVER_RANDOM_SOLVER_H_
