// -*- mode: c++ -*-

#include <string>
#include <utility>
#include <vector>
#include <iostream>

#include "random_solver.h"

std::vector<Move> RandomMoveSolver::Solve(Board board) const {
  assert(random_move_);
  std::vector<Move> moves;
  while (!board.IsFinished()) {
    bool found_move = false;
    while (!found_move) {
      Move next_move = random_move_();
      Board next_board = board.Apply(next_move);
      if (!next_board.IsEmpty()) {
        moves.push_back(next_move);
        board = std::move(next_board);
        found_move = true;
      }
    }
    int round;
    std::tie(board, round) = std::move(board).Unlock();
  }
  return moves;
}

std::vector<Move> RandomMoveSolverWithoutLocking::Solve(Board board) const {
  assert(random_move_);
  std::vector<Move> moves;
  auto current = std::make_unique<Board>(board);
  std::vector<Move> valid_moves = rotations_ ? ValidMoves() : ValidMovesWithoutRotations();
  while (!current->IsFinished()) {
    std::vector<Move> non_error;
    std::vector<Move> non_lock;    
    for (auto move : valid_moves) {
      auto res = current->WillLockOrError(move);
      if (!std::get<1>(res)) {
        non_error.push_back(move);
        if (!std::get<0>(res)) {
          non_lock.push_back(move);
        }
      }
    }
    if (non_error.empty()) {
      moves.clear();
      current = std::make_unique<Board>(board);
      continue;
    }
    Move next_move = non_lock.empty() ? random_move_(non_error) : random_move_(non_lock);
    *current = std::move(*current).Apply(next_move);
    assert(!current->IsEmpty());
    moves.push_back(next_move);
    int round;
    std::tie(*current, round) = std::move(*current).Unlock();
  }
  return moves;
}

std::vector<Move> RandomDummySolver::Solve(Board board) const {
  assert(random_move_);
  std::vector<Move> moves;
  while (!board.IsFinished()) {
    Move next_move = random_move_();
    board = std::move(board).Apply(next_move);
    assert(!board.IsEmpty());
    moves.push_back(next_move);
    int round;
    std::tie(board, round) = std::move(board).Unlock();
  }
  return moves;
}

std::string RandomEncodingOptimizer::Encode(const std::vector<Move> &moves) const {
  assert(random_encoding_);
  std::string result;
  for (Move move : moves) {
    assert(GetMoveEncodings().count(move) > 0);
    result.push_back(random_encoding_(move));
  }
  return result;
}

std::string DummyEncodingOptimizer::Encode(const std::vector<Move> &moves) const {
  std::string result;
  for (Move move : moves) {
    assert(GetMoveEncodings().count(move) > 0);
    result.push_back(GetMoveEncodings().at(move)[0]);
  }
  return result;
}
