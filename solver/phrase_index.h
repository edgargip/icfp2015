// -*- mode: c++ -*-

#ifndef _SOLVER_PHRASE_INDEX_H_
#define _SOLVER_PHRASE_INDEX_H_

#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "../objs/game.h"
#include "trie.h"

class PhraseIndex {
public:
  struct PhraseInfo {
    PhraseInfo(std::string phrase, std::vector<Move> moves)
      : phrase(std::move(phrase)), moves(std::move(moves)) {}

    std::string phrase;
    std::vector<Move> moves;
  };

  using StringTrie = Trie<std::string, std::shared_ptr<PhraseInfo>>;
  using MoveTrie = Trie<std::vector<Move>, std::shared_ptr<PhraseInfo>>;

  static std::vector<Move> PhraseToMoves(const std::string &phrase);

  PhraseIndex() = default;

  void SetPhrases(const std::vector<std::string> &phrases);

  const StringTrie &PhrasesByName() const { return phrases_by_name_; }
  const MoveTrie &PhrasesByMoves() const { return phrases_by_moves_; }

private:
  StringTrie phrases_by_name_;
  MoveTrie phrases_by_moves_;
};

#endif  // _SOLVER_PHRASE_INDEX_H_
