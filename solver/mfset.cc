#include "mfset.h"

#include <map>
#include <numeric>

MFSet::MFSet(int size)
  : parent_(size) {
  std::iota(parent_.begin(), parent_.end(), 0);
}

int MFSet::Cluster(int element) const {
  while (parent_[element] != element) {
    element = parent_[element];
  }
  parent_[element] = element;
  return element;
}

int MFSet::Merge(int element1, int element2) {
  int parent1 = Cluster(element1);
  int parent2 = Cluster(element2);
  if (parent1 == parent2) {
    return parent1;
  } else if (parent1 < parent2) {
    parent_[parent2] = parent1;
    parent_[element2] = parent1;
    return parent1;
  } else {
    parent_[parent1] = parent2;
    parent_[element1] = parent2;
    return parent2;
  }
}

std::vector<std::vector<int>> MFSet::Clusters() const {
  std::map<int, std::vector<int>> clusters_map;
  for (int i = 0; i < Size(); ++i) {
    clusters_map[Cluster(i)].push_back(i);
  }
  std::vector<std::vector<int>> result;
  result.reserve(clusters_map.size());
  std::transform(clusters_map.begin(), clusters_map.end(),
                 std::back_inserter(result),
                 [](const auto &p) { return std::move(p.second); });
  return result;
}
