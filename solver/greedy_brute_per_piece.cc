// -*- mode: c++ -*-

#include <string>
#include <utility>
#include <vector>
#include <iostream>
#include <unordered_set>

#include "greedy_brute_per_piece.h"
struct BestFoundT {
  Board board;
  std::vector<Move> moves;
  int score;
  int max_min_y;
  int max_max_y;
  int n_perimeter;
  int n_hole;  
};

auto DoLevelPieceBacktrack(int level, const Board& board, std::unordered_set<size_t>& hashes, std::vector<Move>& current_moves, BestFoundT& best_found) {
  // std::cout << "Level: " << level << " Best Score: " << std::get<2>(best_found) << "Y: " << std::get<3>(best_found) << std::endl;
  assert(current_moves.size() >= level);
  if (board.IsEmpty()) {  // Error
    return;
  }
  if (board.IsLocked()) {
    auto max_min_y = std::min_element(board.ActiveUnit().members.begin(), board.ActiveUnit().members.end(), [](auto& c1, auto& c2) {
      return c1.y < c2.y;
    })->y;
    auto max_max_y = std::max_element(board.ActiveUnit().members.begin(), board.ActiveUnit().members.end(), [](auto& c1, auto& c2) {
      return c1.y < c2.y;
    })->y;
    auto previous_board = board;    
    auto board_score = board.Unlock();
    auto hole = std::get<0>(board_score).GetHole();
    auto perimeter = std::get<0>(board_score).GetPerimeter();
    auto new_comp = std::make_tuple(std::get<1>(board_score), max_min_y, max_max_y, -hole, -perimeter);
    auto&& old_comp = std::tie(best_found.score, best_found.max_min_y, best_found.max_max_y, best_found.n_hole, best_found.n_perimeter);
    if (new_comp > old_comp) {
      old_comp = new_comp;
      best_found.board = std::move(previous_board);
      best_found.moves = std::vector<Move>(current_moves.begin(), current_moves.begin() + level);
    }
  }
  auto unit_hash = board.ActiveUnit().Hash();
  if (hashes.count(unit_hash)) {
    // Visited this position before.
    return;
  }
  hashes.insert(unit_hash);
  if (current_moves.size() == level) { 
    current_moves.push_back(Move::S);
  }
  auto v = ValidMoves();
  std::random_shuffle(v.begin(), v.end()); 
  for (auto move : v) {
    current_moves[level] = move;
    DoLevelPieceBacktrack(level + 1, board.Apply(move), hashes, current_moves, best_found);      
  }
}

auto DoPieceBacktrack(Board board) {
  BestFoundT best_found;
  best_found.score = -1;
  best_found.n_perimeter = -board.Width() * board.Height() * 6;
  best_found.max_min_y = -1; 
  best_found.max_max_y = -1;
  best_found.n_hole = -100000;     
  std::vector<Move> current_moves;
  std::unordered_set<size_t> hashes;
  DoLevelPieceBacktrack(0, board, hashes, current_moves, best_found);      
  return std::make_tuple(std::move(best_found.board), std::move(best_found.moves));
}


std::vector<Move> GreedyBruteForcePerPieceSolver::Solve(Board board) const {
  std::vector<Move> moves;
  while (!board.IsFinished()) {
    int round;
    auto board_moves = DoPieceBacktrack(board);
    assert(std::get<0>(board_moves).IsLocked());
    moves.insert(moves.end(), std::get<1>(board_moves).begin(), std::get<1>(board_moves).end());    
    board = std::move(std::get<0>(board_moves));
    std::tie(board, round) = std::move(board).Unlock();  
  }
  return moves;
}
