// -*- mode: c++ -*-

#ifndef _SOLVER_SOLVER_H_
#define _SOLVER_SOLVER_H_

#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "../objs/game.h"

class Solver {
public:
  static std::unique_ptr<Solver> Make(const std::string &name = std::string());

  virtual ~Solver() = default;

  virtual void SetPowerPhrases(std::vector<std::string> power_phrases) = 0;

  virtual std::string Solve(Board board) const = 0;
};

class MoveSolver {
public:
  virtual ~MoveSolver() = default;

  virtual std::vector<Move> Solve(Board board) const = 0;
};

class EncodingOptimizer {
public:
  virtual ~EncodingOptimizer() = default;

  virtual void SetPowerPhrases(const std::vector<std::string> power_phrases) = 0;

  virtual std::string Encode(const std::vector<Move> &moves) const = 0;
};

class TwoStepSolver : public Solver {
public:
  TwoStepSolver(std::unique_ptr<MoveSolver> move_solver,
                std::unique_ptr<EncodingOptimizer> encoding_optimizer)
    : move_solver_(std::move(move_solver)),
      encoding_optimizer_(std::move(encoding_optimizer)) {}

  void SetPowerPhrases(std::vector<std::string> power_phrases) override {
    encoding_optimizer_->SetPowerPhrases(std::move(power_phrases));
  }

  std::string Solve(Board board) const override {
    return encoding_optimizer_->Encode(move_solver_->Solve(std::move(board)));
  }

private:
  std::unique_ptr<MoveSolver> move_solver_;
  std::unique_ptr<EncodingOptimizer> encoding_optimizer_;
};

#endif  // _SOLVER_SOLVER_H_
