// -*- mode: c++ -*-

#ifndef _SOLVER_BT_ENCODING_OPTIMIZER_H_
#define _SOLVER_BT_ENCODING_OPTIMIZER_H_

#include <limits>
#include <memory>
#include <string>
#include <vector>

#include "phrase_index.h"
#include "solver.h"

class BtEncodingOptimizer : public EncodingOptimizer {
public:
  void SetPowerPhrases(const std::vector<std::string> power_phrases) override;

  std::string Encode(const std::vector<Move> &moves) const override;

private:
  struct BTState {
    BTState(const std::vector<Move> &moves, int start, int end)
      : moves(moves), start(start), end(end) {}

    const std::vector<Move> &moves;
    int start;
    int end;

    std::string best_encoding;
    int best_score = std::numeric_limits<int>::min();

    std::string current_encoding;
  };

  std::vector<std::pair<int, int>>
  FindSuboptizimationRanges(const std::vector<Move> &moves) const;

  std::string
  EncodeRange(const std::vector<Move> &moves, int start, int end) const;

  void
  EncodeRangeRec(BTState &bt_state, int current, int score,
                 const std::vector<const PhraseIndex::StringTrie::Node *>
                     open_nodes) const;


  PhraseIndex phrase_index_;
};

#endif  // _SOLVER_BT_ENCODING_OPTIMIZER_H_
