// -*- mode: c++ -*-

#ifndef _SOLVER_TRIE_H_
#define _SOLVER_TRIE_H_

#include <experimental/optional>
#include <memory>
#include <tuple>
#include <unordered_map>
#include <vector>

template <typename K, typename V>
class Trie {
public:
  using key_type = K;
  using value_type = V;
  using element_type = typename key_type::value_type;

  class Node {
  public:
    bool HasValue() const { return !!value_; }
    bool HasChildren() const { return !children_.empty(); }
    const value_type &Value() const { return value_.value(); }
    const Node *Next(const element_type &element) const;

  private:
    Node *Ensure(const element_type &element);

    std::experimental::optional<value_type> value_;
    std::unordered_map<element_type, std::unique_ptr<Node>> children_;
    friend class Trie;
  };

  using Matches = std::vector<std::tuple<int, int, const Node *>>;

  const Node *Root() const { return root_.get(); }

  const Node *Insert(const key_type &key, const value_type &value);

  const Node *Insert(const key_type &key, value_type &&value);

  const Node *Find(const key_type &key) const;

  Matches FindAll(const key_type &key) const;

  void Clear();

private:
  std::unique_ptr<Node> root_ = std::make_unique<Node>();
};

template <typename K, typename V>
const typename Trie<K, V>::Node *Trie<K,V>::
Insert(const key_type &key, const value_type &value) {
  Node *current = root_.get();
  for (const element_type &element : key) {
    current = current->Ensure(element);
  }
  current->value_ = value;
  return current;
}

template <typename K, typename V>
const typename Trie<K, V>::Node *Trie<K,V>::
Insert(const key_type &key, value_type &&value) {
  Node *current = root_.get();
  for (const element_type &element : key) {
    current = current->Ensure(element);
  }
  current->value_ = std::move(value);
  return current;
}

template <typename K, typename V>
const typename Trie<K, V>::Node *Trie<K,V>::
Find(const key_type &key) const {
  const Node *current = root_.get();
  for (const element_type &element : key) {
    current = current->Next(element);
    if (current == nullptr) return nullptr;
  }
  return current;
}

template <typename K, typename V>
typename Trie<K,V>::Matches Trie<K,V>::FindAll(const key_type &key) const {
  Matches matches;
  std::vector<std::pair<int, const Node *>> open_nodes = {{0, Root()}};
  int i = 0;
  for (const element_type &element : key) {
    std::vector<std::pair<int, const Node *>> next_nodes;
    for (const auto &start_and_node : open_nodes) {
      const Node *next_node = start_and_node.second->Next(element);
      if (next_node != nullptr) {
        if (next_node->HasValue()) {
          matches.emplace_back(start_and_node.first, i + 1, next_node);
        }
        if (next_node->HasChildren()) {
          next_nodes.emplace_back(start_and_node.first, next_node);
        }
      }
    }
    next_nodes.emplace_back(++i, Root());
    open_nodes = std::move(next_nodes);
  }
  return matches;
}

template <typename K, typename V>
const typename Trie<K, V>::Node *Trie<K,V>::Node::
Next(const element_type &element) const {
  auto it = children_.find(element);
  return it == children_.end() ? nullptr : it->second.get();
}

template <typename K, typename V>
typename Trie<K, V>::Node *Trie<K,V>::Node::
Ensure(const element_type &element) {
  auto it = children_.find(element);
  return (it == children_.end() ?
          children_.emplace(element, std::make_unique<Node>()).first->second :
          it->second).get();
}

template <typename K, typename V>
void Trie<K, V>::Clear() {
  root_ = std::make_unique<Node>();
}

#endif  // _SOLVER_TRIE_H_
