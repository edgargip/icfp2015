#include <cassert>
#include <iostream>
#include <map>
#include <string>
#include <random>
#include <utility>

#include "../objs/game.h"
#include "solver.h"
#include "random_solver.h"

int main() {
  std::string err;
  auto s = json11::Json::parse(R"(
    {
      "id": 1,
      "units": [ {"members": [{"x":0, "y":0}], "pivot": {"x":0, "y":0}},
                 {"members": [{"x":0, "y":0}], "pivot": {"x":0, "y":0}}],
      "width": 5,
      "height": 5,
      "filled": [{"x":0, "y":0}],
      "sourceLength": 7,
      "sourceSeeds": [1, 2, 3]
    }
  )", err);
  const Game game = Game::from_json(s);

  std::unique_ptr<Solver> solver = Solver::Make();

  for (int i = 0; i < game.NumBoards(); ++i) {
    std::cout << "Playing board #" << i << std::endl;

    std::string encoded_moves = solver->Solve(game.GetInitialBoard(i));

    std::cout << "Replaying moves" << std::endl;

    Board board = game.GetInitialBoard(i);
    std::cout << board.Dump() << std::endl;
    int score = 0;
    int next_move_index = 0;
    while (!board.IsFinished()) {
      assert(next_move_index < encoded_moves.size());
      const Move next_move = GetInverseEncoding().at(encoded_moves[next_move_index]);
      std::cerr << "Move #" << next_move_index << ": " 
                << static_cast<int>(next_move) << " ('"
                << encoded_moves[next_move_index] << "')" << std::endl;
      board = std::move(board).Apply(next_move);
      int round;
      std::tie(board, round) = std::move(board).Unlock();
      score += round;
      std::cout << board.Dump() << std::endl;
      ++next_move_index;
    }
    assert(next_move_index == encoded_moves.size());
    std::cout << "Moves: " << encoded_moves << std::endl;
    std::cout << "Score: " << score << std::endl;
  }

  return 0;
}
