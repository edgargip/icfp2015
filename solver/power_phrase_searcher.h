// -*- mode: c++ -*-

#ifndef _SOLVER_POWER_PHRASE_SEARCHER_H_
#define _SOLVER_POWER_PHRASE_SEARCHER_H_

#include "solver.h"

class PowerPhraseSearcher : public Solver {
public:
  void SetPowerPhrases(std::vector<std::string> power_phrases) override;

  std::string Solve(Board board) const override;

private:
  std::vector<std::string> power_phrases_;
  mutable int next_phrase_ = 0;
};

#endif  // _SOLVER_POWER_PHRASE_SEARCHER_H_
