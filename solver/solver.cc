#include <cassert>
#include <memory>
#include <random>

#include "bt_encoding_optimizer.h"
#include "power_phrase_searcher.h"
#include "random_solver.h"
#include "greedy_brute_per_piece.h"
#include "solver.h"

constexpr int kSeed1 = 42;
constexpr int kSeed2 = 24;

std::unique_ptr<Solver> Solver::Make(const std::string &name) {
  if (name == "") {
    return std::make_unique<TwoStepSolver>(
      std::make_unique<GreedyBruteForcePerPieceSolver>(),
      std::make_unique<BtEncodingOptimizer>());
  
  } else if (name == "random-nolock-rotate") {
    return std::make_unique<TwoStepSolver>(
        std::make_unique<RandomMoveSolverWithoutLocking>(
            std::mt19937{kSeed1}, true),
        std::make_unique<BtEncodingOptimizer>());
  } else if (name == "power-phrase-searcher") {
    return std::make_unique<PowerPhraseSearcher>();
  } else {
    assert(false);
  }
}
