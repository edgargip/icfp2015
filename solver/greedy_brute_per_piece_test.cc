#include <cassert>
#include <iostream>
#include <map>
#include <string>
#include <random>
#include <utility>

#include "../objs/game.h"
#include "greedy_brute_per_piece.h"

int main() {
  std::string err;
  auto s = json11::Json::parse(R"(
    {
      "id": 1,
      "units": [ {"members": [{"x":0, "y":0}], "pivot": {"x":0, "y":0}},
                 {"members": [{"x":0, "y":0}], "pivot": {"x":0, "y":0}}],
      "width": 5,
      "height": 5,
      "filled": [{"x":0, "y":0}],
      "sourceLength": 7,
      "sourceSeeds": [1, 2, 3]
    }
  )", err);
  const Game game = Game::from_json(s);

  GreedyBruteForcePerPieceSolver solver;
  for (int i = 0; i < game.NumBoards(); ++i) {
    std::cout << "Playing board #" << i << std::endl;

    auto moves = solver.Solve(game.GetInitialBoard(i));

    std::cout << "Replaying moves" << std::endl;

    Board board = game.GetInitialBoard(i);
    std::cout << board.Dump() << std::endl;
    int score = 0;
    int next_move_index = 0;
    while (!board.IsFinished()) {
      assert(next_move_index < moves.size());
      std::cerr << "Move #" << next_move_index << ": " 
                << static_cast<int>(moves[next_move_index]) << std::endl;
      board = std::move(board).Apply(moves[next_move_index]);
      int round;
      std::tie(board, round) = std::move(board).Unlock();
      score += round;
      std::cout << board.Dump() << std::endl;
      ++next_move_index;
    }
    assert(next_move_index == moves.size());
    std::cout << "Score: " << score << std::endl;
  }

  return 0;
}
