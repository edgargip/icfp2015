#include <iostream>
#include <memory>
#include <random>
#include <string>
#include <vector>

#include "bt_encoding_optimizer.h"
#include "random_solver.h"
#include "../objs/game.h"

namespace std {

std::string to_string(const std::vector<Move> &moves) {
  std::string result;
  for (Move move : moves) {
    if (!result.empty()) result += '/';
    result += GetRepresentation(move);
  }
  return result;
}

}  // namespace std

constexpr int kSeed = 42;

const std::vector<std::string> kPowerPhrases = {
  "ei!",
  "ia! ia!",
  "r'lyeh",
  "yuggoth",
  "cthulu",
  "satan",
  "vancouver",
  "icfp 2015",
};

int main() {
  const std::vector<std::vector<Move>> move_sequences = {
    { Move::E, Move::E, Move::E },  // (nothing)
    { Move::E, Move::SW, Move::W },  // (ei!)
    { Move::E, Move::SW, Move::W, Move::E, Move::SW, Move::W },  // (ei!ei!)
    { Move::E, Move::SW, Move::E, Move::SW, Move::W, Move::E,
      Move::SW },  // (..ei!..)
    { Move::E, Move::CW, Move::SW, Move::SE, Move::E, Move::SE, Move::CCW,
      Move::CW, Move::E, Move::CW, Move::W },  // (.vancouver.)
  };

  const std::vector<std::pair<
      std::string, std::shared_ptr<EncodingOptimizer>>> optimizers = {
    {"Random",
     std::make_unique<RandomEncodingOptimizer>(std::mt19937{kSeed})},
    {"Dummy", std::make_unique<DummyEncodingOptimizer>()},
    {"BT", std::make_unique<BtEncodingOptimizer>()},
  };
  for (const auto &optimizer : optimizers) {
    optimizer.second->SetPowerPhrases(kPowerPhrases);
  }

  for (const auto &moves : move_sequences) {
    std::cout << "* " << std::to_string(moves) << std::endl;
    for (const auto &optimizer : optimizers) {
      std::string encoding = optimizer.second->Encode(moves);
      std::cout << "  * " << optimizer.first << ": \""
                << encoding << "\"" << std::endl;
    }
  }

  return 0;
}
