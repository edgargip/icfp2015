#include <cassert>
#include <string>

#include "trie.h"

int main() {
  Trie<std::string, int> trie;
  trie.Insert("abc", 1);
  trie.Insert("abcd", 2);
  trie.Insert("bcd", 3);
  trie.Insert("aabcd", 4);

  assert(trie.Find("a") != nullptr);
  assert(!trie.Find("a")->HasValue());

  assert(trie.Find("ab") != nullptr);
  assert(!trie.Find("ab")->HasValue());

  assert(trie.Find("abc") != nullptr);
  assert(trie.Find("abc")->HasValue());
  assert(trie.Find("abc")->Value() == 1);

  assert(trie.Find("abcd") != nullptr);
  assert(trie.Find("abcd")->HasValue());
  assert(trie.Find("abcd")->Value() == 2);

  assert(trie.Find("abcde") == nullptr);
}
