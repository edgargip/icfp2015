#include "power_phrase_searcher.h"

#include <vector>
#include <iostream>

#include "../objs/game.h"
#include "phrase_index.h"

void PowerPhraseSearcher::
SetPowerPhrases(std::vector<std::string> power_phrases) {
  power_phrases_ = std::move(power_phrases);
}

std::string PowerPhraseSearcher::Solve(Board board) const {
  if (power_phrases_.empty()) return "";

  int starting_phrase = next_phrase_;
  do {
    // Consider the next phrase.
    assert(next_phrase_ >= 0 && next_phrase_ < power_phrases_.size());
    const std::string &phrase = power_phrases_[next_phrase_];
    std::vector<Move> moves = PhraseIndex::PhraseToMoves(phrase);
    next_phrase_ = (next_phrase_ + 1) % power_phrases_.size();

    // Check it is valid.
    Board current = board;
    bool success = true;
    for (Move next_move : moves) {
      if (board.IsFinished()) {
        success = false;
        break;
      }

      board = board.Apply(next_move);
      if (board.IsEmpty()) {
        success = false;
        break;
      }

      int round;
      std::tie(board, round) = std::move(board).Unlock();
    }

    // Return it if it was valid;
    if (success) return phrase;

    // Try the next one, until we have tried them all.
  } while (next_phrase_ != starting_phrase);
  return "";
}
