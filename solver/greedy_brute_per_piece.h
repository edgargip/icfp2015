#ifndef _SOLVER_GREEDY_BRUTE_PER_PIECE_H_
#define _SOLVER_GREEDY_BRUTE_PER_PIECE_H_

#include <string>
#include <vector>

#include "../objs/game.h"

#include "solver.h"

class GreedyBruteForcePerPieceSolver : public MoveSolver {
public:
  GreedyBruteForcePerPieceSolver() {}

  std::vector<Move> Solve(Board board) const override;
};

#endif
