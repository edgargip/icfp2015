#include <cassert>
#include <iostream>
#include <vector>

#include "mfset.h"

int main() {
  MFSet mf_set(10);
  mf_set.Merge(1, 2);
  mf_set.Merge(3, 4);
  mf_set.Merge(5, 9);
  mf_set.Merge(3, 9);
  mf_set.Merge(1, 6);

  const std::vector<std::vector<int>> kExpectedClusters = {
    {0},
    {1, 2, 6},
    {3, 4, 5, 9},
    {7},
    {8},
  };
  std::vector<std::vector<int>> clusters = mf_set.Clusters();
  for (const auto &cluster : clusters) {
    std::cout << "{ ";
    for (int v : cluster) {
      std::cout << v << " ";
    }
    std::cout << "} ";
  }
  std::cout << std::endl;
  assert(clusters == kExpectedClusters);
  return 0;
}
