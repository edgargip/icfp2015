#include "phrase_index.h"

#include <algorithm>
#include <iterator>
#include <memory>
#include <string>
#include <vector>

#include "../objs/game.h"

/* static */ std::vector<Move>
PhraseIndex::PhraseToMoves(const std::string &phrase) {
  const auto &inverse_encoding = GetInverseEncoding();
  std::vector<Move> moves;
  moves.reserve(phrase.size());
  std::transform(phrase.begin(), phrase.end(), std::back_inserter(moves),
                 [&inverse_encoding](char ch) {
                   return inverse_encoding.at(ch);
                 });
  std::remove(moves.begin(), moves.end(), Move::S);
  return moves;
}

void PhraseIndex::SetPhrases(const std::vector<std::string> &phrases) {
  phrases_by_name_.Clear();
  phrases_by_moves_.Clear();

  for (const std::string &phrase : phrases) {
    std::vector<Move> moves = PhraseToMoves(phrase);
    auto phrase_info =
        std::make_shared<PhraseInfo>(phrase, std::move(moves));
    phrases_by_name_.Insert(phrase, phrase_info);
    phrases_by_moves_.Insert(phrase_info->moves, phrase_info);
  }
}
