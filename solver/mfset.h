// -*- mode: c++ -*-

#ifndef _SOLVER_MFSET_H_
#define _SOLVER_MFSET_H_

#include <vector>

class MFSet {
public:
  MFSet(int size);

  int Size() const { return parent_.size(); }

  int Cluster(int element) const;

  int Merge(int element1, int element2);

  std::vector<std::vector<int>> Clusters() const;

private:
  mutable std::vector<int> parent_;
};

#endif  // _SOLVER_MFSET_H_
