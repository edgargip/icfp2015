#include "../opts/opts.h"
#include "../solver/solver.h"
#include "../objs/game.h"
#include "../objs/output.h"
#include "../tools/reader.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int main(int argc, char** argv) {
  cxxopts::Options options;
  options
    .add_option("f", "", "File", cxxopts::value<std::vector<std::string>>())
    .add_option("p", "", "Power", cxxopts::value<std::vector<std::string>>())
    .add_option("t", "", "Time", cxxopts::value<int>()->default_value("-1"))
    .add_option("m", "", "Memory", cxxopts::value<int>()->default_value("-1"))
    .add_option("", "tag", "Tag",
                cxxopts::value<std::string>()->default_value("C++Turbo"))
    .add_option("s", "solver", "Solver", cxxopts::value<std::string>());
  options.parse(argc, argv);
  const auto& files = options.count("f")
      ? options["f"].as<std::vector<std::string>>()
      : std::vector<std::string>();
  const auto& power_phrases = options.count("p")
      ? options["p"].as<std::vector<std::string>>()
      : std::vector<std::string>();

  int time = options["t"].as<int>();
  int memory = options["m"].as<int>();
  auto solver = Solver::Make(options["s"].as<std::string>());
  solver->SetPowerPhrases(power_phrases);

  std::vector<Game> games;
  for (auto& file : files) {
    games.push_back(ReadFromFile<Game>(file));
  }
  Output output;
  for (auto& game : games) {
    for (int i = 0; i < game.source_seeds.size(); ++i) {
      auto commands = solver->Solve(game.GetInitialBoard(i));
      output.outputs.push_back(OutputPerSeed{
          game.id, game.source_seeds[i], options["tag"].as<std::string>(),
          std::move(commands)});
    }
  }
  std::cout << output.to_json().dump() << '\n';
  return 0;
}
