/* Copyright (c) 2013 Dropbox, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "json11.h"

#include <cassert>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <limits>

namespace json11 {

// Internal class hierarchy - JsonValue objects are not exposed to users of this
// API.
class JsonValue {
 protected:
  friend class Json;
  friend class JsonInt;
  friend class JsonDouble;
  virtual Json::Type type() const = 0;
  virtual bool equals(const JsonValue& other) const = 0;
  virtual bool less(const JsonValue& other) const = 0;
  virtual void dump(std::string& out) const = 0;
  virtual double as_number() const;
  virtual int as_int() const;
  virtual bool as_bool() const;
  virtual const std::string& as_string() const;
  virtual const Json::array& as_array() const;
  virtual const Json& operator[](size_t i) const;
  virtual const Json::object& as_object() const;
  virtual const Json& operator[](const std::string& key) const;
  virtual ~JsonValue() {}
};

// TODO(ivan): I do not think we need more than 10 depth, we will see.
constexpr int max_depth = 200;

using std::string;
using std::vector;
using std::map;
using std::make_shared;
using std::initializer_list;
using std::move;

// Serialization
static void dump(std::nullptr_t, string& out) { out += "null"; }

static void dump(double value, string& out) {
  if (std::isfinite(value)) {
    char buf[32];
    snprintf(buf, sizeof(buf), "%.17g", value);
    out += buf;
  } else {
    out += "null";
  }
}

static void dump(int value, string& out) {
  char buf[32];
  snprintf(buf, sizeof buf, "%d", value);
  out += buf;
}

static void dump(bool value, string& out) { out += value ? "true" : "false"; }

static void dump(const string& value, string& out) {
  out += '"';
  for (size_t i = 0; i < value.length(); i++) {
    const char ch = value[i];
    if (ch == '\\') {
      out += "\\\\";
    } else if (ch == '"') {
      out += "\\\"";
    } else if (ch == '\b') {
      out += "\\b";
    } else if (ch == '\f') {
      out += "\\f";
    } else if (ch == '\n') {
      out += "\\n";
    } else if (ch == '\r') {
      out += "\\r";
    } else if (ch == '\t') {
      out += "\\t";
    } else if (static_cast<uint8_t>(ch) <= 0x1f) {
      char buf[8];
      snprintf(buf, sizeof buf, "\\u%04x", ch);
      out += buf;
    } else if (static_cast<uint8_t>(ch) == 0xe2 && i + 2 < value.length() &&
               static_cast<uint8_t>(value[i + 1]) == 0x80 &&
               static_cast<uint8_t>(value[i + 2]) == 0xa8) {
      out += "\\u2028";
      i += 2;
    } else if (static_cast<uint8_t>(ch) == 0xe2 && i + 2 < value.length() &&
               static_cast<uint8_t>(value[i + 1]) == 0x80 &&
               static_cast<uint8_t>(value[i + 2]) == 0xa9) {
      out += "\\u2029";
      i += 2;
    } else {
      out += ch;
    }
  }
  out += '"';
}

static void dump(const Json::array& values, string& out) {
  bool first = true;
  out += "[";
  for (const auto& value : values) {
    if (!first) out += ", ";
    value.dump(out);
    first = false;
  }
  out += "]";
}

static void dump(const Json::object& values, string& out) {
  bool first = true;
  out += "{";
  for (const auto& kv : values) {
    if (!first) out += ", ";
    dump(kv.first, out);
    out += ": ";
    kv.second.dump(out);
    first = false;
  }
  out += "}";
}

void Json::dump(string& out) const { m_ptr->dump(out); }

template <Json::Type tag, typename T>
class Value : public JsonValue {
 protected:
  explicit Value(T value) : m_value(move(value)) {}

  Json::Type type() const override { return tag; }

  bool equals(const JsonValue& other) const override {
    // TODO(ivan): This is UB, but it is checked later, static_cast is ok!
    return m_value == static_cast<const Value<tag, T>&>(other).m_value;
  }
  bool less(const JsonValue& other) const override {
    // TODO(ivan): This is UB, but it is checked later, static_cast is ok!
    return m_value < static_cast<const Value<tag, T>&>(other).m_value;
  }

  const T m_value;
  void dump(string& out) const override { json11::dump(m_value, out); }
};

class JsonDouble : public Value<Json::kNumber, double> {
  double as_number() const override { return m_value; }
  int as_int() const override { return static_cast<int>(m_value); }
  bool equals(const JsonValue& other) const override {
    return m_value == other.as_number();
  }
  bool less(const JsonValue& other) const override {
    return m_value < other.as_number();
  }

 public:
  explicit JsonDouble(double value) : Value(value) {}
};

class JsonInt : public Value<Json::kNumber, int> {
  double as_number() const override { return m_value; }
  int as_int() const override { return m_value; }
  bool equals(const JsonValue& other) const override {
    return m_value == other.as_number();
  }
  bool less(const JsonValue& other) const override {
    return m_value < other.as_number();
  }

 public:
  explicit JsonInt(int value) : Value(value) {}
};

class JsonBoolean : public Value<Json::kBool, bool> {
  bool as_bool() const override { return m_value; }

 public:
  explicit JsonBoolean(bool value) : Value(value) {}
};

class JsonString : public Value<Json::kString, string> {
  const string& as_string() const override { return m_value; }

 public:
  explicit JsonString(string value) : Value(move(value)) {}
};

class JsonArray : public Value<Json::kArray, Json::array> {
  const Json::array& as_array() const override { return m_value; }
  const Json& operator[](size_t i) const override;

 public:
  explicit JsonArray(Json::array value) : Value(move(value)) {}
};

class JsonObject : public Value<Json::kObject, Json::object> {
  const Json::object& as_object() const override { return m_value; }
  const Json& operator[](const string& key) const override;

 public:
  explicit JsonObject(Json::object value) : Value(move(value)) {}
};

class JsonNull : public Value<Json::kNull, std::nullptr_t> {
 public:
  JsonNull() : Value(nullptr) {}
};

// Static globals - static-init-safe
struct Statics {
  const std::shared_ptr<JsonValue> null = make_shared<JsonNull>();
  const std::shared_ptr<JsonValue> t = make_shared<JsonBoolean>(true);
  const std::shared_ptr<JsonValue> f = make_shared<JsonBoolean>(false);
  const string empty_string;
  const Json::array empty_array;
  const Json::object empty_object;
  Statics() {}
};

const Statics& statics() {
  static const Statics s{};
  return s;
}

const Json& static_null() {
  // This has to be separate, not in Statics, because Json() accesses
  // statics().null.
  static const Json json_null;
  return json_null;
}

// Constructors
Json::Json() noexcept : m_ptr(statics().null) {}
Json::Json(std::nullptr_t) noexcept : m_ptr(statics().null) {}
Json::Json(double value) : m_ptr(make_shared<JsonDouble>(value)) {}
Json::Json(int value) : m_ptr(make_shared<JsonInt>(value)) {}
Json::Json(bool value) : m_ptr(value ? statics().t : statics().f) {}
Json::Json(string value) : m_ptr(make_shared<JsonString>(move(value))) {}
Json::Json(Json::array values) : m_ptr(make_shared<JsonArray>(move(values))) {}
Json::Json(Json::object values)
    : m_ptr(make_shared<JsonObject>(move(values))) {}

// Accessors
Json::Type Json::type() const { return m_ptr->type(); }
double Json::as_number() const { return m_ptr->as_number(); }
int Json::as_int() const { return m_ptr->as_int(); }
bool Json::as_bool() const { return m_ptr->as_bool(); }
const string& Json::as_string() const { return m_ptr->as_string(); }
const Json::array& Json::as_array() const { return m_ptr->as_array(); }
const Json::object& Json::as_object() const { return m_ptr->as_object(); }
const Json& Json::operator[](size_t i) const { return (*m_ptr)[i]; }
const Json& Json::operator[](const string& key) const { return (*m_ptr)[key]; }

double JsonValue::as_number() const { return 0.; }
int JsonValue::as_int() const { return 0; }
bool JsonValue::as_bool() const { return false; }
const string& JsonValue::as_string() const { return statics().empty_string; }
const Json::array& JsonValue::as_array() const { return statics().empty_array; }
const Json::object& JsonValue::as_object() const {
  return statics().empty_object;
}
const Json& JsonValue::operator[](size_t) const { return static_null(); }
const Json& JsonValue::operator[](const string&) const { return static_null(); }

const Json& JsonObject::operator[](const string& key) const {
  auto iter = m_value.find(key);
  return (iter == m_value.end()) ? static_null() : iter->second;
}
const Json& JsonArray::operator[](size_t i) const {
  if (i >= m_value.size())
    return static_null();
  else
    return m_value[i];
}

// Comparison
bool Json::operator==(const Json& other) const {
  if (m_ptr->type() != other.m_ptr->type()) return false;
  return m_ptr->equals(*other.m_ptr);
}

bool Json::operator<(const Json& other) const {
  if (m_ptr->type() != other.m_ptr->type())
    return m_ptr->type() < other.m_ptr->type();
  return m_ptr->less(*other.m_ptr);
}

// Parsing

// Format char c suitable for printing in an error message.
static inline string esc(char c) {
  char buf[12];
  if (static_cast<uint8_t>(c) >= 0x20 && static_cast<uint8_t>(c) <= 0x7f) {
    snprintf(buf, sizeof(buf), "'%c' (%d)", c, c);
  } else {
    snprintf(buf, sizeof(buf), "(%d)", c);
  }
  return string(buf);
}

constexpr bool in_range(long x, long lower, long upper) {
  return (x >= lower && x <= upper);
}

// JsonParser
// Object that tracks all state of an in-progress parse.
struct JsonParser {
  const string& str;
  size_t i;
  string& err;
  bool failed;

  // Mark this parse as failed.
  Json fail(string msg) { return fail(move(msg), Json()); }

  template <typename T>
  T fail(string msg, T err_ret) {
    if (!failed) err = std::move(msg);
    failed = true;
    return err_ret;
  }

  // Advance until the current character is non-whitespace.
  void consume_whitespace() {
    while (i < str.size() &&
           (str[i] == ' ' || str[i] == '\r' || str[i] == '\n' ||
            str[i] == '\t' || str[i] == std::char_traits<char>::eof()))
      ++i;
  }

  // Return the next non-whitespace character. If the end of the input is
  // reached,
  // flag an error and return 0.
  char get_next_token() {
    consume_whitespace();
    if (i == str.size()) return fail("unexpected end of input", 0);
    return str[i++];
  }

  // Encode pt as UTF-8 and add it to out.
  void encode_utf8(long pt, string& out) {
    if (pt < 0) return;
    if (pt < 0x80) {
      out += static_cast<char>(pt);
    } else if (pt < 0x800) {
      out += static_cast<char>((pt >> 6) | 0xC0);
      out += static_cast<char>((pt & 0x3F) | 0x80);
    } else if (pt < 0x10000) {
      out += static_cast<char>((pt >> 12) | 0xE0);
      out += static_cast<char>(((pt >> 6) & 0x3F) | 0x80);
      out += static_cast<char>((pt & 0x3F) | 0x80);
    } else {
      out += static_cast<char>((pt >> 18) | 0xF0);
      out += static_cast<char>(((pt >> 12) & 0x3F) | 0x80);
      out += static_cast<char>(((pt >> 6) & 0x3F) | 0x80);
      out += static_cast<char>((pt & 0x3F) | 0x80);
    }
  }

  // Parse a string, starting at the current position.
  string parse_string() {
    string out;
    long last_escaped_codepoint = -1;
    while (true) {
      if (i == str.size()) return fail("unexpected end of input in string", "");

      char ch = str[i++];
      if (ch == '"') {
        encode_utf8(last_escaped_codepoint, out);
        return out;
      }

      if (in_range(ch, 0, 0x1f))
        return fail("unescaped " + esc(ch) + " in string", "");

      // The usual case: non-escaped characters
      if (ch != '\\') {
        encode_utf8(last_escaped_codepoint, out);
        last_escaped_codepoint = -1;
        out += ch;
        continue;
      }

      // Handle escapes
      if (i == str.size()) return fail("unexpected end of input in string", "");

      ch = str[i++];

      if (ch == 'u') {
        // Extract 4-byte escape sequence
        string esc = str.substr(i, 4);
        if (esc.length() < 4) {
          return fail("bad \\u escape: " + esc, "");
        }
        for (int j = 0; j < 4; j++) {
          if (!in_range(esc[j], 'a', 'f') && !in_range(esc[j], 'A', 'F') &&
              !in_range(esc[j], '0', '9'))
            return fail("bad \\u escape: " + esc, "");
        }
        long codepoint = strtol(esc.data(), nullptr, 16);

        // JSON specifies that characters outside the BMP shall be encoded as a
        // pair
        // of 4-hex-digit \u escapes encoding their surrogate pair components.
        // Check
        // whether we're in the middle of such a beast: the previous codepoint
        // was an
        // escaped lead (high) surrogate, and this is a trail (low) surrogate.
        if (in_range(last_escaped_codepoint, 0xD800, 0xDBFF) &&
            in_range(codepoint, 0xDC00, 0xDFFF)) {
          // Reassemble the two surrogate pairs into one astral-plane character,
          // per
          // the UTF-16 algorithm.
          encode_utf8((((last_escaped_codepoint - 0xD800) << 10) |
                       (codepoint - 0xDC00)) +
                          0x10000,
                      out);
          last_escaped_codepoint = -1;
        } else {
          encode_utf8(last_escaped_codepoint, out);
          last_escaped_codepoint = codepoint;
        }
        i += 4;
        continue;
      }

      encode_utf8(last_escaped_codepoint, out);
      last_escaped_codepoint = -1;

      if (ch == 'b') {
        out += '\b';
      } else if (ch == 'f') {
        out += '\f';
      } else if (ch == 'n') {
        out += '\n';
      } else if (ch == 'r') {
        out += '\r';
      } else if (ch == 't') {
        out += '\t';
      } else if (ch == '"' || ch == '\\' || ch == '/') {
        out += ch;
      } else {
        return fail("invalid escape character " + esc(ch), "");
      }
    }
  }

  // Parse a double.
  Json parse_number() {
    size_t start_pos = i;

    if (str[i] == '-') i++;

    // Integer part
    if (str[i] == '0') {
      i++;
      if (in_range(str[i], '0', '9'))
        return fail("leading 0s not permitted in numbers");
    } else if (in_range(str[i], '1', '9')) {
      i++;
      while (in_range(str[i], '0', '9')) i++;
    } else {
      return fail("invalid " + esc(str[i]) + " in number");
    }

    if (str[i] != '.' && str[i] != 'e' && str[i] != 'E' &&
        (i - start_pos) <=
            static_cast<size_t>(std::numeric_limits<int>::digits10)) {
      return std::atoi(str.c_str() + start_pos);
    }

    // Decimal part
    if (str[i] == '.') {
      i++;
      if (!in_range(str[i], '0', '9'))
        return fail("at least one digit required in fractional part");

      while (in_range(str[i], '0', '9')) i++;
    }

    // Exponent part
    if (str[i] == 'e' || str[i] == 'E') {
      i++;

      if (str[i] == '+' || str[i] == '-') i++;

      if (!in_range(str[i], '0', '9'))
        return fail("at least one digit required in exponent");

      while (in_range(str[i], '0', '9')) i++;
    }

    return std::strtod(str.c_str() + start_pos, nullptr);
  }

  // Expect that 'str' starts at the character that was just read. If it does,
  // advance
  Json expect(const string& expected, Json res) {
    assert(i != 0);
    i--;
    if (str.compare(i, expected.length(), expected) == 0) {
      i += expected.length();
      return res;
    } else {
      return fail("parse error: expected " + expected + ", got " +
                  str.substr(i, expected.length()));
    }
  }

  // Parse a JSON object.
  Json parse_json(int depth) {
    if (depth > max_depth) {
      return fail("exceeded maximum nesting depth");
    }

    char ch = get_next_token();
    if (failed) return Json();

    if (ch == '-' || (ch >= '0' && ch <= '9')) {
      i--;
      return parse_number();
    }

    if (ch == 't') return expect("true", true);
    if (ch == 'f') return expect("false", false);
    if (ch == 'n') return expect("null", Json());
    if (ch == '"') return parse_string();

    if (ch == '{') {
      Json::object data;
      ch = get_next_token();
      if (ch == '}') return data;

      while (1) {
        if (ch != '"') return fail("expected '\"' in object, got " + esc(ch));

        string key = parse_string();
        if (failed) return Json();

        ch = get_next_token();
        if (ch != ':') return fail("expected ':' in object, got " + esc(ch));

        data[std::move(key)] = parse_json(depth + 1);
        if (failed) return Json();

        ch = get_next_token();
        if (ch == '}') break;
        if (ch != ',') return fail("expected ',' in object, got " + esc(ch));
        ch = get_next_token();
      }
      return data;
    }
    if (ch == '[') {
      Json::array data;
      ch = get_next_token();
      if (ch == ']') return data;

      while (1) {
        i--;
        data.push_back(parse_json(depth + 1));
        if (failed) return Json();

        ch = get_next_token();
        if (ch == ']') break;
        if (ch != ',') return fail("expected ',' in list, got " + esc(ch));
        ch = get_next_token();
        (void)ch;
      }
      return data;
    }
    return fail("expected value, got " + esc(ch));
  }
};

Json Json::parse(const string& in, string& err) {
  JsonParser parser{in, 0, err, false};
  Json result = parser.parse_json(0);

  // Check for any trailing garbage
  parser.consume_whitespace();
  if (parser.i != in.size())
    return parser.fail("unexpected trailing " + esc(in[parser.i]));

  return result;
}

vector<Json> Json::parse_multi(const string& in, string& err) {
  JsonParser parser{in, 0, err, false};

  vector<Json> json_vec;
  while (parser.i != in.size() && !parser.failed) {
    json_vec.push_back(parser.parse_json(0));
    // Check for another object
    parser.consume_whitespace();
  }
  return json_vec;
}

// Shape-checking
bool Json::has_shape(const shape& types, string& err) const {
  if (!is_object()) {
    err = "expected JSON object, got " + dump();
    return false;
  }
  for (const auto& item : types) {
    if ((*this)[item.first].type() != item.second) {
      err = "bad type for " + item.first + " in " + dump();
      return false;
    }
  }
  return true;
}

}  // namespace json11

