#ifndef _TOOLS_RAND_H_
#define _TOOLS_RAND_H_

// A random generator
static_assert(sizeof(unsigned) == 4, "");
constexpr const unsigned kMultiplier = 1103515245;
constexpr const unsigned kIncrement = 12345;

inline auto GetRand(unsigned seed) {
  return [seed]() mutable {
    auto r = (seed & 0x7FFFFFFFU) >> 16U;
    seed = kMultiplier * seed + kIncrement;
    return r;
  };
}

#endif  // _TOOLS_RAND_H_
