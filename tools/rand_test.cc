#include "rand.h"

#include <cassert>

int main() {
  auto r = GetRand(17);
  assert(r() == 0);
  assert(r() == 24107);
  assert(r() == 16552);
  assert(r() == 12125);
  assert(r() == 9427);
  assert(r() == 13152);
  assert(r() == 21440);
  assert(r() == 3383);
  assert(r() == 6873);
  assert(r() == 16117);
  return 0;
}

