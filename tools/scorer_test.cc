#include "scorer.h"

#include <cassert>
#include <iostream>

int main() {
  auto s = MoveScorer();
  std::cout << "=============" << std::endl;
  std::cout << s(1, 1) << std::endl;
  std::cout << s(1, 1) << std::endl;
  std::cout << s(1, 1) << std::endl;
  std::cout << s(1, 1) << std::endl;
  std::cout << s(1, 1) << std::endl;
  std::cout << "=============" << std::endl;
  auto p = PowerScorer("ei!");
  std::cout << p(0) << std::endl;
  std::cout << p(1) << std::endl;
  std::cout << p(2) << std::endl;
  std::cout << p(3) << std::endl;
  return 0;
}

