#ifndef _TOOLS_SCORER_H_
#define _TOOLS_SCORER_H_

#include <string>

// A random generator
inline auto MoveScorer() {
  auto ls_old = 0U;
  return [ls_old](unsigned size, unsigned ls) mutable {
    auto r = size + 100U * ls * (ls + 1U) / 2U;
    if (ls_old > 1U)  r += r * (ls_old - 1U) / 10U;
    ls_old = ls;
    return r;
  };
}

inline auto PowerScorer(std::string phrase) {
  const auto len = phrase.size();
  return [len](unsigned rep) {
    return (rep > 0U) ? 2U * len * rep + 300U : 0U;
  };
}

#endif  // _TOOLS_RAND_H_
