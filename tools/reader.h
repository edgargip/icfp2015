#ifndef _TOOLS_READER_H_
#define _TOOLS_READER_H_

#include <fstream>
#include <string>

#include "../json/json11.h"

template <class T>
T ReadFromFile(const std::string& file) {
  std::string contents;
  std::ifstream input(file);
  while (input) {
    std::string buffer;
    std::getline(input, buffer);
    contents += buffer;
  };
  std::string err;
  auto s = json11::Json::parse(contents, err);
  assert(err.empty());
  return T::from_json(s);
}

#endif  // _TOOLS_READER_H_
