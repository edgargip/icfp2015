#!/bin/bash

if (( $# != 1 )); then
    echo "Usage: $0 <tag>"
    exit -1
fi
TAG="$1"
shift 1

for i in $(seq 0 23);
do
    echo "Sending problem ${i}..."
    ./send.sh "solution_${i}.json" "${TAG}"
done
