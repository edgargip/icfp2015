#!/bin/bash

for f in solution_*.json; do
    echo \
        "${f}: " \
        "$(../tests/visualizer -s -q -i ${f/solution/problem} -o ${f} -p "ei!" -p "ia! ia!" -p "r'lyeh" -p "yuggoth" -p "cthulhu cthulhu" | tail -n 1)"
done
