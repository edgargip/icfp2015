use v5.10;
use strict;
use warnings;

use JSON::PP;

sub clone {
    my ($value) = @_;
    given (ref($value)) {
        when ('') {
            return $value;
        }
        when ('ARRAY') {
            return [map { clone($_) } @{$value}];
        }
        when ('HASH') {
            return {map { $_ => clone($value->{$_}) } keys(%{$value})};
        }
        default {
            die "Bad reference type: $_";
        }
    }
}

die if @ARGV != 2;
my ($inputFile, $outputPrefix) = @ARGV;

open(my $fin, '<', $inputFile) or die;
my $inputJson = join('', <$fin>);
close($fin);

my $input = decode_json($inputJson);
for (my $i = 0; $i < @{$input}; ++$i) {
    my $inputCopy = clone($input);
    for (my $j = 0; $j < @{$input}; ++$j) {
        if ($i == $j) {
            $inputCopy->[$j]{'tag'} .=
                sprintf('-%d.%d', $input->[$j]{'problemId'},
                        $input->[$j]{'seed'});
        } else {
            $inputCopy->[$j]{'solution'} = ',';
        }
    }

    my $outputFile = "$outputPrefix.$i.json";
    open(my $fout, '>', $outputFile) or die;
    print $fout encode_json($inputCopy);
    close($fout);
    print "Generated $outputFile\n";
}
