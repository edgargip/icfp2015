#!/bin/bash

# Not phrases: "cthulu", "icfp", "vancouver"

declare -a PHRASES
PHRASES[0]="ei!"
PHRASES[1]="ia! ia!"
PHRASES[2]="r'lyeh"
PHRASES[3]="yuggoth"
PHRASES[4]="ph'nglui mglw'nafh cthulhu r'lyeh wgah'nagl fhtagn!"

for phrase in "${PHRASES[@]}"; do
    echo "${phrase}"
    grep -l "${phrase}" solution_*.json
    echo
done
