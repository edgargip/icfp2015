use strict;
use warnings;

use JSON::PP;

my $teamName = "C++ Turbo";

print "Fetching data...\n";
my $rankingsJs = `wget -O - https://davar.icfpcontest.org/rankings.js 2> /dev/null`;
$rankingsJs =~ s/^var data = //;

print "Parsing data...\n";
my $data = decode_json($rankingsJs);
my @teamScores =
    map { sprintf("Global\t\tRank: %3d\tScore: %5d\tPower: %5d\tTags: %s\n",
                  $_->{"rank"}, $_->{"score"}, $_->{"power_score"},
                  join(',', @{$_->{"tags"}})) }
    grep { $_->{"team"} eq $teamName } @{$data->{"data"}{"rankings"}};

foreach my $setting (@{$data->{"data"}{"settings"}}) {
    push(@teamScores,
         map { sprintf("Problem %d\tRank: %3d\tScore: %5d\tPower: %5d\tTags: %s\n",
                       $setting->{"setting"}, $_->{"rank"}, $_->{"score"},
                       $_->{"power_score"}, join(',', @{$_->{"tags"}})) }
         grep { $_->{"team"} eq $teamName } @{$setting->{"rankings"}});
}

print "Output:\n\n";
print foreach @teamScores;
