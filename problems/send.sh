#!/bin/bash

API_TOKEN=mnXxXL46VmE1rPKZ3z5bChJf96cfqGOquDhluzwQciY=
TEAM_ID=298

if (( $# != 1 && $# != 2 )); then
    echo "Usage: $0 <file> <tag>?"
    exit -1
fi
INPUT="$1"
TAG="$2"

if [[ -z $TAG ]]; then
    curl --user ":${API_TOKEN}" -X POST \
        -H "Content-Type: application/json" \
        -d "@${INPUT}" \
        "https://davar.icfpcontest.org/teams/${TEAM_ID}/solutions"
else
    curl --user ":${API_TOKEN}" -X POST \
        -H "Content-Type: application/json" \
        -d @<(cat "${INPUT}" | sed -e 's/C++Turbo/C++Turbo-'${TAG}'/g') \
        "https://davar.icfpcontest.org/teams/${TEAM_ID}/solutions"
fi

echo
